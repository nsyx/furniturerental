package data;

/**
 * Builds queries
 * @author Justin
 *
 */
public class QueryBuilder {
	
	public final static String CUSTOMER_ATTRIBUTES[] = new String[] {
			"cid=? ",
			"fname=? ",
			"lname=? ",
			"phone=? ",
	};
	
	public final static String FURNITURE_ATTRIBUTES[] = new String[] {
			"fid=? ",
			"style=? ",
			"category=? ",
			"description=? ",
	};
	
	public final static String INVENTORY_ITEM_ATTRIBUTES[] = new String[] {
			"inventoryID=? ",
			"quantity=? "
	};
	
	public final static String RENTAL_ATTRIBUTES[] = new String[] {
			"customerID=? "
	};
	
	public final static String CUSTOMER_TABLE = "`customer`";
	public final static String FURNITURE_TABLE = "`furniture`";
	public final static String INVENTORY_ITEM_TABLE = "`inventory_item`";
	public final static String RENTAL_TABLE = "`rental_transaction`";
	
	
	/**
	 * Construct a SQL query based on arguments.
	 * @param args the params for the sql query
	 * @param table the table the query operates on
	 * @return the query
	 */
	public static String getSelectQuery(String[] args, String table) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + table + " WHERE ");
		String queries[];
		
		if(table.equals(CUSTOMER_TABLE)) {
			queries = CUSTOMER_ATTRIBUTES;
		} else if(table.contentEquals(FURNITURE_TABLE)) {
			queries = FURNITURE_ATTRIBUTES;
		} else if(table.contentEquals(INVENTORY_ITEM_TABLE)) {
			queries = INVENTORY_ITEM_ATTRIBUTES;
		}  else if(table.contentEquals(RENTAL_TABLE)) {
			queries = RENTAL_ATTRIBUTES;
		} else {
			throw new UnsupportedOperationException("Encountered "
					+ "an unsupported table when attempting to build SQL query");
		}
		String s = "";
		for(int i=0;i<args.length;i++) {
			if(!args[i].isBlank()) {
				s = s + (queries[i]);
			}
		}
		s = s.trim().replaceAll(" ", " AND ");
		if(s.contentEquals("")) {
			sb.append("true");
		} else {
			sb.append(s);
		}
		return sb.toString();
	}
}
