package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import model.Furniture;
import model.InventoryItem;

/**
 * Handles queries related to furniture
 * @author Justin
 *
 */
public class FurnitureQuery extends QueryHandler {

	/**
	 * Executes a search of furniture using the specified params
	 * @param args the params for the search
	 * @return the results of the search
	 * @throws SQLException
	 */
	public ResultSet executeFurnitureSearch(String[] args) throws SQLException {
		return this.executeTableSearch(args, QueryBuilder.FURNITURE_TABLE);
	}

	/**
	 * Executes a search of inventory using the specified params
	 * @param args the params for the search
	 * @return the results of the search
	 * @throws SQLException
	 */
	public ResultSet executeInventoryItemSearch(String[] args) throws SQLException {
		return this.executeTableSearch(args, QueryBuilder.INVENTORY_ITEM_TABLE);
	}

	/**
	 * Gets the specified furniture's entry in the inventory table
	 * @param furniture the furniture to get from inventory
	 * @return the corresponding entry in the inventory table
	 * @throws SQLException
	 */
	public InventoryItem getInventoryItem(Furniture furniture) throws SQLException {
		int iid = furniture.getIid();
		ResultSet rs = executeInventoryItemSearch(new String[] { iid + "" });
		int qty = 0;
		while (rs.next()) {
			qty = rs.getInt(2);
		}
		closeConnection();
		return new InventoryItem(qty, iid);
	}
}
