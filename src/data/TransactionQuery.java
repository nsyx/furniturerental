package data;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Furniture;
import model.FurnitureCheckOuts;
import model.FurnitureReturns;
import model.FurnitureTransaction;

/**
 * Handles queries relating to transactions
 * @author Justin and Laura
 *
 */
public class TransactionQuery extends QueryHandler {

	/**
	 * Generates a list of furniture returns based off of the specified rental transaction ID
	 * @param rTransID the rental transaction ID
	 * @return a list of furniture returns
	 */
	public ArrayList<FurnitureReturns> getFurnitureReturns(int rTransID) {
		ResultSet rs = getReturnTransactions(rTransID);
		ArrayList<FurnitureReturns> fr = new ArrayList<>();
		try {
			while (rs.next()) {
				fr.add(new FurnitureReturns(
						new Furniture(rs.getInt("fid"), rs.getInt("inventoryID"), rs.getString("category"),
								rs.getString("style"), rs.getString("description"), rs.getDouble("dailyRate"),
								rs.getDouble("lateFee")),
						null, rs.getDate("dueDate"), rs.getInt("quantity"), rTransID, rs.getInt("daysLate"),
						rs.getInt("retQuant")));
			}
		} catch (SQLException e) {
			return null;
		}
		closeConnection();
		return fr;
	}

	/**
	 * Gets the returns of the specified rental
	 * @param rTransID the rental to find returns of
	 * @return the corresponding returns
	 */
	public ResultSet getReturnTransactions(int rTransID) {
		String query = "SELECT ri.quantity, f.*, rt.dueDate, \n"
				+ "IF(rt.dueDate < CURRENT_DATE, DATEDIFF(CURRENT_DATE, rt.dueDate), 0) as daysLate, \n"
				+ "IFNUll(sum(ret.quantity), 0) as retQuant \n" + "FROM `rental_item` as ri \n"
				+ "inner join `furniture` as f on ri.itemID = f.inventoryID \n"
				+ "inner join `rental_transaction` as rt on ri.rTransID = rt.rentalTransactionID \n"
				+ "left join `return_item` as ret on ri.itemID = ret.itemID and ri.rTransID = ret.rentalTransID \n"
				+ "WHERE rTransID = ?\n" + "GROUP BY ri.itemID, ri.rTransID;";
		PreparedStatement stmt = null;
		try {
			establishConnection();
			Connection connection = getConnection();
			stmt = connection.prepareStatement(query);
			stmt.setInt(1, rTransID);
			stmt.execute();
			return stmt.getResultSet();
		} catch (SQLException e) {
			return null;
		}
	}
	
	/**
	 * Gets the returns of the specified customer
	 * @param customerID the ID of the customer under inspection
	 * @return the results of the query
	 */
	public ResultSet getCustomersReturns(int customerID) {
		String query = "SELECT t4.description, t1.rentalDateTime, DATEDIFF(t5.returnDateTime, t1.rentalDateTime) AS daysRented, t2.quantity " + 
				"FROM rental_transaction t1 " + 
				"INNER JOIN return_item t2 ON t2.rentalTransID=t1.rentalTransactionID " + 
				"INNER JOIN customer t3 ON t3.cid=t1.customerID " + 
				"INNER JOIN furniture t4 ON t4.inventoryID=t2.itemID " + 
				"INNER JOIN return_transaction t5 ON t2.returnTransID=t5.returnTransactionID "
				+ "WHERE t3.cid=?";
		
		PreparedStatement stmt = null;
		try {
			establishConnection();
			Connection connection = getConnection();
			stmt = connection.prepareStatement(query);
			stmt.setInt(1, customerID);
			stmt.execute();
			return stmt.getResultSet();
		} catch (SQLException e) {
			return null;
		}
	}
	
	/**
	 * Gets the rental history of the specified inventory item
	 * @param inventoryID the ID of the inventory item
	 * @return the rental history of the inventory item
	 */
	public ResultSet getFurnitureHistory(int inventoryID) {
		String query = "SELECT concat(t1.fname, \" \", t1.lname) as customerName, concat(t5.fname, \" \", t5.lname) AS overseeingEmployee, t2.rentalDateTime, t4.quantity, t4.rTransID " + 
				"FROM customer t1 " + 
				"INNER JOIN rental_transaction t2 ON t2.customerID=t1.cid " + 
				"INNER JOIN employee t5 ON t5.eID=t2.eid " + 
				"INNER JOIN rental_item t4 ON t4.rTransID=t2.rentalTransactionID "
				+ "WHERE t4.itemID=?";
		PreparedStatement stmt = null;
		try {
			establishConnection();
			Connection connection = getConnection();
			stmt = connection.prepareStatement(query);
			stmt.setInt(1, inventoryID);
			stmt.execute();
			return stmt.getResultSet();
		} catch (SQLException e) {
			return null;
		}
	}

	/**
	 * Rents all items in the specified list of checkouts.
	 * @param checkOut the list of checkouts
	 * @return whether the operation was successful
	 * @throws SQLException
	 */
	public boolean rentItems(List<FurnitureCheckOuts> checkOut) throws SQLException {

		for (FurnitureCheckOuts i : checkOut) {
			if (i.getFurnitureItem().getOnHand() < i.getQuantity()) {
				return false;
			}
		}
		int transID = this.updateRentalTransactions(checkOut);
		if (transID == 0) {
			return false;
		}
		for (FurnitureCheckOuts i : checkOut) {
			if (!this.updateInventoryItems(i, true)) {
				return false;
			}
			if (!this.updateRentalItems(i, transID)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * returns the specified items
	 * @param returns the items to return
	 * @param rentalID the rental ID for return_item entries
	 * @return whether the operation was successful
	 * @throws SQLException
	 */
	public boolean returnItems(List<FurnitureReturns> returns, int rentalID) throws SQLException {
		int transID = this.updateReturnTransactions(returns);
		if (transID == 0) {
			return false;
		}
		for (FurnitureReturns i : returns) {
			if (!this.updateInventoryItems(i, false)) {
				return false;
			}
			if (!this.updateReturnItems(i, transID, rentalID)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Updates the return_items table
	 * @param i the furniture returned
	 * @param transID the ID of the return transaction
	 * @param rentalID the ID of the rental transaction
	 * @return whether the operation was successful
	 */
	private boolean updateReturnItems(FurnitureReturns i, int transID, int rentalID) {
		String query = "INSERT INTO `return_item`(`rentalTransID`, `itemID`, `returnTransID`, `quantity`) VALUES (?,?,?,?)";
		int iid = i.getFurnitureItem().getIid();
		int qty = i.getQuantity() - i.getReturnedQuantity();
		if (qty <= 0) {
			return true;
		}

		PreparedStatement stmt = null;
		try {
			establishConnection();
			Connection connection = getConnection();
			stmt = connection.prepareStatement(query);

			stmt.setInt(1, rentalID);
			stmt.setInt(2, iid);
			stmt.setInt(3, transID);
			stmt.setInt(4, qty);
			stmt.execute();
			return true;
		} catch (SQLException e) {
			return false;
		} finally {
			closeConnection();
		}
	}

	/**
	 * Updates the quantity of the specified item in the inventory table
	 * @param i the item
	 * @param isRental whether it is a rental
	 * @return whether the operation was successful
	 * @throws SQLException
	 */
	private boolean updateInventoryItems(FurnitureTransaction i, boolean isRental) throws SQLException {
		String query = "? ?";
		if (isRental) {
			query = "UPDATE `inventory_item` SET quantity = quantity - ? WHERE inventoryID = ?";

		} else {
			query = "UPDATE `inventory_item` SET quantity = quantity + ? WHERE inventoryID = ?";
		}
		int qty = i.getQuantity();
		// int iID = this.getInventoryID(i);
		int iID = getFurnitureQuery().getInventoryItem(i.getFurnitureItem()).getId();

		PreparedStatement stmt = null;
		try {
			establishConnection();
			Connection connection = getConnection();
			stmt = connection.prepareStatement(query);

			stmt.setInt(1, qty);
			stmt.setInt(2, iID);
			stmt.execute();
			return true;
		} catch (SQLException e) {
			return false;
		} finally {
			closeConnection();
		}
	}

	/**
	 * Updates the return_transactions table
	 * @param returns the items being returned
	 * @return the id of this entry in return_transactions
	 * @postcondition return_transactions += one entry
	 */
	private int updateReturnTransactions(List<FurnitureReturns> returns) {
		String query = "INSERT INTO `return_transaction`(`returnTransactionID`, `eid`, `returnDateTime`) VALUES (null, ?,?)";
		int eid = returns.get(0).getOverseeingEmployee().getEmployeeID();
		Timestamp rDate = new Timestamp(System.currentTimeMillis());
		int retID = 0;

		PreparedStatement stmt = null;

		try {
			establishConnection();
			Connection connection = getConnection();
			stmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			stmt.setInt(1, eid);
			stmt.setTimestamp(2, rDate);
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				retID = rs.getInt(1);
			}
			return retID;
		} catch (SQLException e) {
			e.printStackTrace();
			return retID;
		} finally {
			closeConnection();
		}
	}

	/**
	 * Updates the rental_transactions table
	 * @param checkOut the items being rented
	 * @return the id of this entry in rental_transactions
	 * @postcondition rental_transactions += one entry
	 */
	private int updateRentalTransactions(List<FurnitureCheckOuts> checkOut) {
		int cid = checkOut.get(0).getCustomer().getCid();
		int eid = checkOut.get(0).getOverseeingEmployee().getEmployeeID();
		Timestamp transDate = checkOut.get(0).getCheckoutDate();
		Date dueDate = checkOut.get(0).getDueDate();
		int rTransID = 0;

		PreparedStatement stmt = null;

		String query = ("INSERT INTO `rental_transaction` (`rentalTransactionID`, `customerID`, `eid`, `rentalDateTime`, `dueDate`) "
				+ "VALUES (NULL, ?, ?, ?, ?);");

		try {
			establishConnection();
			Connection connection = getConnection();
			stmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			stmt.setInt(1, cid);
			stmt.setInt(2, eid);
			stmt.setTimestamp(3, transDate);
			stmt.setDate(4, dueDate);
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				rTransID = rs.getInt(1);
			}
			return rTransID;
		} catch (SQLException e) {
			e.printStackTrace();
			return rTransID;
		} finally {
			closeConnection();
		}

	}

	/**
	 * Updates the rental_items table
	 * @param i the item rented
	 * @param rTransID the rental transaction ID
	 * @postcondition return_items += 1 entry
	 * @return whether the operation was successful
	 * @throws SQLException
	 */
	private boolean updateRentalItems(FurnitureCheckOuts i, int rTransID) throws SQLException {
		String query = "INSERT INTO `rental_item`(`rTransID`, `itemID`, `quantity`) VALUES (?,?,?)";
		int iID = getFurnitureQuery().getInventoryItem(i.getFurnitureItem()).getId();
		int qty = i.getQuantity();

		PreparedStatement stmt = null;
		try {
			establishConnection();
			Connection connection = getConnection();
			stmt = connection.prepareStatement(query);

			stmt.setInt(1, rTransID);
			stmt.setInt(2, iID);
			stmt.setInt(3, qty);
			stmt.execute();
			return true;
		} catch (SQLException e) {
			return false;
		} finally {
			closeConnection();
		}
	}

	/**
	 * Searches for a rental history
	 * @param args the params of the select query
	 * @return the results of the search
	 * @throws SQLException
	 */
	public ResultSet executeRentalHistorySearch(String args[]) throws SQLException {
		return this.executeTableSearch(args, QueryBuilder.RENTAL_TABLE);
	}

	/**
	 * Searches for a return history
	 * @param args the params of the select query
	 * @return the results of the search
	 * @throws SQLException
	 */
	public ResultSet executeReturnHistorySearch(int customerID) {
		return this.getCustomersReturns(customerID);
	}
	
	public ResultSet executeFurnitureHistorySearch(int inventoryID) throws SQLException {
		return this.getFurnitureHistory(inventoryID);
	}
}
