package data;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.cj.exceptions.CJCommunicationsException;

import model.LoginHandler;

/**
 * Handles queries for the current session
 *
 */
public class SessionQuery extends QueryHandler {
	
	/**
	 * Gets a string representation of the current user.
	 * @return a string representation of the current user.
	 * @throws CJCommunicationsException 
	 * @throws ConnectException 
	 */
	public String getCurrentUser() {
		String query = ("SELECT fname, lname FROM employee WHERE username=? ;");
		try {
			establishConnection();
			Connection connection = getConnection();
			PreparedStatement stmt = connection.prepareStatement(query);
			
			stmt.setString(1, LoginHandler.getSessionToken().getUsername());
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			StringBuilder sb = new StringBuilder();
			while(rs.next()) {
				sb.append(rs.getString(1));
				sb.append(" ");
				sb.append(rs.getString(2));
			}
			return sb.toString();
		} catch (SQLException e) {
			return "";
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * Gets an employee's ID based on their username
	 * @param username the employee's username
	 * @return the employee's ID
	 */
	public int getEmployeeIDByUsername(String username) {
		int eID = -1;
		String query = ("SELECT eID FROM employee WHERE username=?;");
		try {
			establishConnection();
			Connection connection = getConnection();
			PreparedStatement stmt = connection.prepareStatement(query);
			
			stmt.setString(1, username);
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			rs.next();
			
			eID = rs.getInt(1);	
		} catch (SQLException e) {
			return -1;
		} finally {
			closeConnection();
		}
		return eID;
	}
	
	/**
	 * Determines if login is valid
	 * @param username the username under login
	 * @param password the password with login
	 * @param secret a secret
	 * @return whether the username and password are valid
	 */
	public boolean authenticate(String username, String password, String secret) {
		
		String query = ("SELECT * FROM users WHERE username=? AND password=AES_ENCRYPT(?, ?);");
		try {
			establishConnection();
			Connection connection = getConnection();
			PreparedStatement stmt = connection.prepareStatement(query);
			
			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.setString (3, secret);
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * Asks the server if the current user is an administrator.
	 * @return true if admin, no otherwise.
	 * @throws CJCommunicationsException 
	 * @throws ConnectException 
	 */
	public boolean isUserAdmin(String user) {
		String query = ("SELECT isAdmin FROM employee WHERE username=?;");
		try {
			establishConnection();
			Connection connection = getConnection();
			PreparedStatement stmt = connection.prepareStatement(query);
			
			stmt.setString(1, user);
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			rs.next();
			String s = rs.getString(1);
			if(s.equals("1")) {
				return true;
			} else {
				return false;
			}
			
		} catch (SQLException e) {
			return false;
		} finally {
			closeConnection();
		}
	}

}
