package data;

import model.Customer;
import model.Employee;
import model.Furniture;

/**
 * Class representing an abstraction layer for managing items and the
 * connection with the database
 * @author Ewan
 *
 */
abstract public class DataSource {

	/**
	 * Adds a customer
	 * @param customer the customer to add
	 * @return whether the operation was successful
	 */
	abstract public boolean addCustomer(Customer customer);
	
	/**
	 * Edits the specified customer
	 * @param customer the customer to edit
	 * @return whether the operation was successful
	 */
	abstract public boolean editCustomer(Customer customer);
	
	/**
	 * Adds a furniture
	 * @param furniture the furniture to add
	 * @return whether the operation was successful
	 */
	abstract public boolean addFurniture(Furniture furniture);
	
	/**
	 * adds an employee
	 * @param employee to add
	 * @return whether the operation was successful
	 */
	abstract public boolean addEmployee(Employee employee);
	
	/**
	 * removes a customer
	 * @param customerID the ID of the customer to remove
	 * @return whether the operation was successful
	 */
	abstract public boolean removeCustomer(int customerID);
	
	/**
	 * removes a furniture
	 * @param furnitureID the ID of the furniture to remove
	 * @return whether the operation was successful
	 */
	abstract public boolean removeFurniture(int furnitureID);
	
	/**
	 * removes an employee
	 * @param employeeID the ID of the employee to remove
	 * @return whether the operation was successful
	 */
	abstract public boolean removeEmployee(int employeeID);
	
	/**
	 * Fetches all customers in the data
	 * @return an array of all customers
	 */
	abstract public Customer[] fetchAllCustomers();
	
	/**
	 * Fetches all employees
	 * @return an array of all employees
	 */
	abstract public Employee[] fetchAllEmployees();
	
	/**
	 * Fetches all furniture
	 * @return an array of all furniture
	 */
	abstract public Furniture[] fetchAllFurniture();
	
	/**
	 * Clears all data from the data source
	 * @return whether the operation was successful
	 */
	abstract public boolean clearAllData();
	
	/**
	 * Runs the specified sql query
	 * @param query the query to run
	 * @return the results of the query in string form
	 */
	abstract public String runSQLQuery(String query);

	/**
	 * determines if the user's login is valid
	 * @param username the username under login
	 * @param password the password with login
	 * @param secret a secret
	 * @return whether the operation was successful
	 */
	abstract public boolean authenticate(String username, String password, String secret);
	
}
