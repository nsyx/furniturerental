package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class for handling database connections.
 */
public class QueryHandler {

	private String connectionString = "jdbc:mysql://160.10.25.16:3306/cs3230f19a?user=cs3230f19a&password=77Fvd8Hotj6qPBqc&serverTimezone=EST&zeroDateTimeBehavior=convertToNull";
	protected Connection connection = null;

	/**
	 * Create a new FurnitureQuery and return it
	 * 
	 * @return a furniture query
	 */
	public FurnitureQuery getFurnitureQuery() {
		return new FurnitureQuery();
	}

	/**
	 * Create a new CustomerQuery and return it
	 * 
	 * @return a customer query
	 */
	public CustomerQuery getCustomerQuery() {
		return new CustomerQuery();
	}

	/**
	 * Create a new SessionQuery and return it
	 * 
	 * @return a session query
	 */
	public SessionQuery getSessionQuery() {
		return new SessionQuery();
	}

	/**
	 * Create a new TransactionQuery and return it
	 * 
	 * @return a transaction query
	 */
	public TransactionQuery getTransactionQuery() {
		return new TransactionQuery();
	}

	/**
	 * Try to establish a connection to the db.
	 */
	public void establishConnection() throws SQLException {
		connection = DriverManager.getConnection(connectionString);
	}

	/**
	 * Try to close a connection to the db.
	 * @return whether the operation was successful.
	 */
	public boolean closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	/**
	 * Executes the specified query
	 * @param query the query to executee
	 * @return the results of the query
	 * @throws SQLException
	 */
	public ResultSet executeAdminQuery(String query) throws SQLException {
		ResultSet rs = null;
		PreparedStatement stmt;
		establishConnection();
		getConnection();
		stmt = connection.prepareStatement(query);
		stmt.execute();
		rs = stmt.getResultSet();
		return rs;
	}

	/**
	 * Executes a search on a table
	 * @param args the parameters of the search
	 * @param table the table to perform the search on
	 * @return the results of the search
	 * @throws SQLException
	 */
	public ResultSet executeTableSearch(String args[], String table) throws SQLException {
		String q = QueryBuilder.getSelectQuery(args, table);
		ResultSet rs = null;
		establishConnection();
		PreparedStatement stmt = connection.prepareStatement(q);

		int argCount = 0;

		for (int i = 0; i < args.length; i++) {
			if (i == 0) {
				if (!args[i].isBlank()) {
					argCount++;
					stmt.setInt(argCount, Integer.parseInt(args[0]));
				}
			} else {
				if (!args[i].isBlank()) {
					argCount++;
					stmt.setString(argCount, args[i]);
				}
			}
		}
		stmt.execute();
		rs = stmt.getResultSet();
		return rs;
	}

	/**
	 * Gets the current connection.
	 * 
	 * @return the current connection.
	 */
	public Connection getConnection() {
		return connection;
	}
}
