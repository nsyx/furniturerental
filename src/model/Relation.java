package model;

public class Relation {
	
	private long recordCreationMilliseconds;
	private long recordEditMilliseconds;
	
	public Relation() {
		this.recordCreationMilliseconds = System.currentTimeMillis();
		this.recordEditMilliseconds = System.currentTimeMillis();
	}

	public long getRecordCreationMilliseconds() {
		return recordCreationMilliseconds;
	}

	public long getRecordEditMilliseconds() {
		return recordEditMilliseconds;
	}

	public void setRecordCreationMilliseconds(long recordCreationMilliseconds) {
		this.recordCreationMilliseconds = recordCreationMilliseconds;
	}

	public void setRecordEditMilliseconds(long recordEditMilliseconds) {
		this.recordEditMilliseconds = recordEditMilliseconds;
	}
	

}
