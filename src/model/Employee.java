package model;

public class Employee extends Relation {
	
	private int employeeID;
	private String username;
	private boolean active;
	
	public Employee(int eid, String us) {
		this.username = us;
		this.employeeID = eid;
	}
	
	public int getEmployeeID() {
		return this.employeeID;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public boolean isActive() {
		return this.active;
	}
}
