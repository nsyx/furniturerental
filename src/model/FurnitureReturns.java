package model;

import java.sql.Date;
import java.sql.Timestamp;

public class FurnitureReturns extends FurnitureTransaction {
	
	private Date dueDate;
	private int rentalTransID;
	private int daysLate;
	private int maxQuantity;
	private int returnedQuantity;
	
	public FurnitureReturns(Furniture furnitureItem, Timestamp returnDate, Date dueDate, int qty, int rentalTransID, int daysLate, int retQty) {
		super(furnitureItem, returnDate, qty);		
		this.setDueDate(dueDate);
		this.setQuantity(qty);
		this.setRTID(rentalTransID);
		this.daysLate = daysLate;
		this.maxQuantity = qty;
		this.returnedQuantity = retQty;
	}

	public int getRTID() {
		return this.rentalTransID;
	}
	
	public void setRTID(int rentalTransID) {
		this.rentalTransID = rentalTransID;
	}

	public Date getDueDate() {
		return this.dueDate;
	}
	
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	/**
	 * Calculates the late fees associated with this return.
	 * @return the late fees associated with this return.
	 */
	public double getLateFees() {
		double lateFees = 0;
		if(this.daysLate != 0) {
			lateFees = (this.getFurnitureItem().getLateFee() + this.getFurnitureItem().getDailyRate()) * (this.getQuantity() - this.returnedQuantity) * this.daysLate;
		}
		
		return lateFees;
	}
	
	public double getLateRate() {
		return this.getFurnitureItem().getLateFee();
	}
	
	public int getFid() {
		return this.getFurnitureItem().getFid();
	}

	public int getMaxQuantity() {
		return maxQuantity;
	}
	
	public int getReturnedQuantity() {
		return this.returnedQuantity;
	}

}
