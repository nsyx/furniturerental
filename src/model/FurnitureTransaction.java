package model;

import java.sql.Timestamp;

public class FurnitureTransaction extends Relation {

	private int transactionID;
	private Employee overseeingEmployee;
	private Furniture furnitureItem;
	private Timestamp transactionDate;
	private int quantity;
	
	/**Stores and retrieves information related to a single furniture checkout item.
	 * 
	 * @Postcondition: as this(furnitureItem, checkout, dueDate, qty), plus this.checkoutID == checkoutID, this.customer == customer, and this.overseeingEmployee == employee
	 * @param checkoutID: The ID for this rental item.
	 * @param customer: The customer renting this item.
	 * @param overseeingEmployee: The employee checking out this customer.
	 * For the other params, see this(furnitureItem, checkout, dueDate, qty).
	 */
	public FurnitureTransaction(int checkoutID, Employee overseeingEmployee, Furniture furnitureItem, Timestamp checkOut) {
		this(furnitureItem, checkOut, 1);
		this.transactionID = checkoutID;
		this.overseeingEmployee = overseeingEmployee;

	}
	
	/**Stores and retrieves information related to a single furniture checkout item.
	 * 
	 * @Postcondition this.furnitureItem == furnitureItem, this.checkoutDate == checkOut, this.dueDate == dueDate, this.quantity == qty
	 * @param furnitureItem: The item of furniture being rented.
	 * @param checkOut: The timestamp of the checkout time.
	 * @param dueDate: The date this rental item is due.
	 * @param qty: The amount of this furniture item being rented.
	 */
	public FurnitureTransaction(Furniture furnitureItem, Timestamp checkOut, int qty) {
		super();		
		this.furnitureItem = furnitureItem;
		this.setTransactionDate(checkOut);
		this.setQuantity(qty);
	}
	
	/**
	 * Gets the id of this checkout.
	 * @return the id of this checkout.
	 */
	public int gettransactionID() {
		return this.transactionID;
	}
	/**
	 * Sets the id of this checkout.
	 * @postcondition: this.checkoutID == checkoutID
	 * @param checkoutID the new checkout ID
	 */
	public void settransactionID(int checkoutID) {
		this.transactionID = checkoutID;
	}
	public Employee getOverseeingEmployee() {
		return overseeingEmployee;
	}
	public void setOverseeingEmployee(Employee overseeingEmployee) {
		this.overseeingEmployee = overseeingEmployee;
	}
	public Furniture getFurnitureItem() {
		return furnitureItem;
	}
	public void setFurnitureItem(Furniture furnitureItem) {
		this.furnitureItem = furnitureItem;
	}
	public Timestamp getTransactionDate() {
		return this.transactionDate;
	}
	public void setTransactionDate(Timestamp checkoutDate) {
		this.transactionDate = checkoutDate;
	}
	@Override
	public String toString() {
		return getFurnitureItem().getDescription();
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return furnitureItem.getType();
	}
	
	/**
	 * @return the style
	 */
	public String getStyle() {
		return furnitureItem.getStyle();
	}

}
