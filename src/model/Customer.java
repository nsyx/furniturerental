package model;

import java.sql.Date;

public class Customer extends Relation {
	
	private int cID;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String address;
	private String city;
	private String apt;
	private String state;
	private String zip;
	private Date dob;

	public Customer(String firstname, String lastname, String phone, 
			String email, String address, String city, String apt, String state, 
			String zip, Date dob) {

		setFirstName(firstname);
		setLastName(lastname);
		setPhone(phone);
		setEmail(email);
		setAddress(address);
		setCity(city);
		setApt(apt);
		setState(state);
		setZip(zip);
		setDob(dob);
	}
	
	public Customer(int cid, String firstname, String lastname, String phone, 
			String email, String address, String city, String apt, String state, 
			String zip, Date dob) {
		
		this(firstname,  lastname,  phone, 
				 email,  address,  city,  apt,  state, 
				 zip,  dob);
		this.cID = cid;
		
	}
	private void setDob(Date _dob) {
		this.dob = _dob;
	}

	private void setZip(String zip) {
		this.zip = zip;
	}

	private void setState(String state) {
		this.state = state;
	}

	private void setApt(String apt) {
		this.apt = apt;
	}

	private void setCity(String city) {
		this.city = city;
	}

	private void setAddress(String address) {
		this.address = address;
	}

	private void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return customer first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param customer first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return customer last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param customer last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the phone number
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param the customer's phone number
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * Generates a customer ID
	 * @return the new customer ID
	 */
	public static int generateCustomerID() {
		//TODO replace with something better
		return 0;
	}

	@Override
	public String toString() {
		return (getLastName() + ", " + getFirstName());
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the apt
	 */
	public String getApt() {
		return apt;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}
	
	public int getCid() {
		return cID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((apt == null) ? 0 : apt.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((dob == null) ? 0 : dob.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Customer))
			return false;
		Customer other = (Customer) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (apt == null) {
			if (other.apt != null)
				return false;
		} else if (!apt.equals(other.apt))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (dob == null) {
			if (other.dob != null)
				return false;
		} else if (!dob.equals(other.dob))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (zip == null) {
			if (other.zip != null)
				return false;
		} else if (!zip.equals(other.zip))
			return false;
		return true;
	}
}
