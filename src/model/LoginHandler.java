package model;

import data.QueryHandler;

public class LoginHandler {
	
	private static String secret = "$$$h_it$_a_s3cr3t";
	private QueryHandler dba;
	private static SessionToken sessionToken;
	private static boolean loggedIn;
	
	/**
	 * @return the sessionToken
	 */
	public static SessionToken getSessionToken() {
		return sessionToken;
	}

	/**
	 * @param sessionToken the sessionToken to set
	 */
	public void setSessionToken(SessionToken sessionToken) {
		LoginHandler.sessionToken = sessionToken;
	}

	public LoginHandler(QueryHandler dba) {
		this.dba = dba;
		sessionToken = null;
		loggedIn = false;
	}
	
	/**
	 * Try to log in. If successful, generate a session token for this log in session.
	 * @param username
	 * @param password
	 * @return true if login successful, false if not.
	 */
	public boolean login(String username, String password) {

		boolean success = dba.getSessionQuery().authenticate(username, password, secret);
		if(success) {
			boolean admin = dba.getSessionQuery().isUserAdmin(username);
			setSessionToken(new SessionToken(username, admin));
			loggedIn = true;
			return true;
		} else {
			loggedIn = false;
			return false;
		}
	}

	public void logout() {
		if (dba.getConnection() != null) {
			dba.closeConnection();
		}
		if (getSessionToken() != null) {
			getSessionToken().invalidate();
		}
		this.loggedIn = false;
		setSessionToken(null);
	}
	
	public static boolean getLoggedIn() {
		return loggedIn;
	}

}
