package controller;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;
import model.Customer;
import model.Furniture;
import model.FurnitureCheckOuts;
import model.ShoppingCart;

/**
 * Handles logic behind the Search Pane
 * @author Justin
 *
 */
public class SearchPaneController {

	/**
	 * Adds the specified item to the cart
	 * @param selected the specified item
	 * @postcondition the cart += the item
	 */
	public void addItemToCart(Furniture selected) {
		
		ShoppingCart cart = ApplicationRegistry.get().getShoppingCart();
		if(!cart.contains(selected)) {
			Timestamp now = new Timestamp(System.currentTimeMillis());
			LocalDate due = LocalDate.now().plusDays(30);
			Date date = Date.valueOf(due);
			FurnitureCheckOuts item = new FurnitureCheckOuts(selected, now, date, 1);
			cart.addItemToCart(item);
		} else {
			throw new IllegalArgumentException("Item is already in cart");
		}
	}
	
	/**
	 * Sets the editing customer to the specified one
	 * @param selected the customer to edit
	 * @postcondition the editing customer == selected
	 */
	public void setEditingCustomer(Customer selected) {
		ApplicationRegistry.get().setEditingCustomer(selected);
	}
	
	/**
	 * Sets the rental IDs to be returned equal to the specified list.
	 * 
	 * @param id the ids to be returned
	 * @postcondition the rental return IDs == id
	 */
	public void setRentalReturnID(List<Integer> id) {
		ApplicationRegistry.get().setRentalReturnID(id);
	}
	
	/**
	 * Adds the specified id to the list of IDs for returns.
	 * 
	 * @param id the new id to be returned
	 * @postcondition the rental return IDs += id
	 */
	public void addRentalReturnID(int rentalID) {		
		ApplicationRegistry.get().addRentalReturnID(rentalID);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void buildTableFromResults(ResultSet rs, ObservableList<Object> tableData, TableView<Object> tableView) throws SQLException {
			int colcount;
			try {
				colcount = rs.getMetaData().getColumnCount();
			} catch(NullPointerException e) {
				return;
			}
			for(int i=0 ; i<colcount; i++){
                final int j = i;                
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i+1));
                col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList,String>,ObservableValue<String>>() {
                    public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {                                                                                              
                    	try {
                    		return new SimpleStringProperty(param.getValue().get(j).toString());
                    	} catch (IndexOutOfBoundsException e) {
                    		return new SimpleStringProperty("");
                    	}
                    }                    
                });
                tableView.getColumns().addAll(col);
            }
			
            while(rs.next()){
                ObservableList<String> row = FXCollections.observableArrayList();
                for(int i=1 ; i<=rs.getMetaData().getColumnCount(); i++){
                	String s = rs.getString(i);
                	if(rs.wasNull() ) {
                		s = "NULL";
                	}
                	row.add(s);
                }
                tableData.add(row);
            }
            tableView.setItems(tableData);
			rs.close();
	}
	
	/**
	 * Builds a customer from the table data
	 * @param object the table data
	 * @return the customer built
	 * @throws ParseException
	 */
	public Customer constructCustomerFromTableData(ObservableList<?> object) throws ParseException {
		Customer customer = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = sdf.parse(object.get(10).toString());
		java.sql.Date sqlDate = new Date(date.getTime());
		customer = new Customer(
				Integer.parseInt(object.get(0).toString()),
				object.get(1).toString(),
				object.get(2).toString(),
				object.get(3).toString(),
				object.get(4).toString(),
				object.get(5).toString(),
				object.get(6).toString(),
				object.get(7).toString(),
				object.get(8).toString(),
				object.get(9).toString(),
				sqlDate);
		return customer;
	}
	
	/**
	 * Builds a furniture from the table data
	 * @param object the table data
	 * @return the furniture built
	 * @throws ParseException
	 */
	public Furniture constructFurnitureFromTableData(ObservableList<?> object) {
		Furniture furniture = null;
		if(object != null) {
			int fid = Integer.parseInt(object.get(0).toString());
			String category = object.get(1).toString();
			String style = object.get(2).toString();
			String description = object.get(3).toString();
			double dailyRate = Double.parseDouble(object.get(4).toString());
			double lateFee = Double.parseDouble(object.get(5).toString());
			int inventoryID = Integer.parseInt(object.get(6).toString());

		furniture = new Furniture(
				fid,
				inventoryID,
				category,
				style,
				description,
				dailyRate,
				lateFee
				);
		}
		return furniture;
	}
	
	/**
	 * Executes the specified customer search
	 * @param args the parameters of the customer search
	 * @return the results of the customer search
	 * @throws SQLException
	 */
	public ResultSet execCustomerSearch(String[] args) throws SQLException {
		return ApplicationRegistry.get().getDatasource().getCustomerQuery().executeCustomerSearch(args);
	}

	/**
	 * Executes the specified furniture search
	 * @param args the parameters of the furniture search
	 * @return the results of the furniture search
	 * @throws SQLException
	 */
	public ResultSet execFurnitureSearch(String[] args) throws SQLException {
		return ApplicationRegistry.get().getDatasource().getFurnitureQuery().executeFurnitureSearch(args);
		
	}

	/**
	 * Sets the customer of the shopping cart equal to the specified customer.
	 * @param customer the customer for the cart
	 * @postcondition the cart's customer == customer
	 */
	public void beginCustomerTransaction(Customer customer) {
		ApplicationRegistry.get().getShoppingCart().setCustomer(customer);
	}

	/**
	 * Gets the customer from the shopping cart
	 * @return the customer from the shopping car
	 */
	public Customer getLoadedCustomer() {
		return ApplicationRegistry.get().getShoppingCart().getCustomer();
	}

	/**
	 * Executes the query from the admin panel
	 * @param text the admin's query
	 * @return the results of the query
	 * @throws SQLException
	 */
	public ResultSet executeAdminQuery(String text) throws SQLException {
		return ApplicationRegistry.get().getDatasource().executeAdminQuery(text);
	}

	/**
	 * Gets the rental history of the specified customer.
	 * @param selected the customer under inspection
	 * @return selected's rental history
	 * @throws SQLException
	 */
	public ResultSet loadRentHistory(Customer selected) throws SQLException {
		return ApplicationRegistry.get().getDatasource().getTransactionQuery()
				.executeRentalHistorySearch(new String[] {selected.getCid()+""});
	}
	
	/**
	 * Gets the return history of the specified customer.
	 * @param selected the customer under inspection
	 * @return selected's return history
	 * @throws SQLException
	 */
	public ResultSet loadReturnHistory(Customer selected) throws SQLException {
		return ApplicationRegistry.get().getDatasource().getTransactionQuery()
				.executeReturnHistorySearch(selected.getCid());
	}
	
	/**
	 * Gets the rental history of the specified furniture.
	 * @param inventoryID the ID of the furniture under inspection
	 * @return the furniture's rental history
	 * @throws SQLException
	 */
	public ResultSet loadFurnitureRentalHistory(int inventoryID)  throws SQLException{
		return ApplicationRegistry.get().getDatasource().getTransactionQuery()
				.executeFurnitureHistorySearch(inventoryID);
	}
}
