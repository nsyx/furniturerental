package controller;

import java.util.ArrayList;
import java.util.List;

import data.QueryHandler;
import model.Customer;
import model.LoginHandler;
import model.ShoppingCart;
import view.GUIManager;

/**
 * Class containing application settings and general functionality parameters
 */
public class ApplicationRegistry {
	
    private static ApplicationRegistry instance;
    
    /**
     * Instantiates a new ApplicationRegistry
     * @param manager the GUIManager
     * @param datasource the source of data
     * @param handler the login handler
     * @param cart the list of furniture items to be rented
     */
    public static void initialize(GUIManager manager, QueryHandler datasource, LoginHandler handler, ShoppingCart cart) {
    	if (manager == null) {
    		throw new IllegalArgumentException("Manager can't be null");
    	}
    	ApplicationRegistry.instance = new ApplicationRegistry(manager, datasource, handler, cart);
    }
	
    /**
     * Gets this instance of the Application Registry
     * @return this instance of the Application Registry
     */
	public static ApplicationRegistry get() {
		return ApplicationRegistry.instance;
	}
	
	/**
	 * Gets whether this instance has been initialized or not.
	 * @return whether this instance has been initialized or not.
	 */
	public static boolean hasInitialized() {
		return (ApplicationRegistry.instance != null);
	}
	
	private QueryHandler datasource;
	private LoginHandler loginHandler;
	private GUIManager guiManager;
	private ShoppingCart shoppingCart;
	private Customer editingCustomer;
	private List<Integer> returningRentalTransIDs;
	
	private ApplicationRegistry(GUIManager manager, QueryHandler datasource, LoginHandler handler, ShoppingCart cart) {
		if (manager == null) {
			throw new IllegalArgumentException("GUIManager can't be null");
		}
		this.datasource = datasource;
		this.loginHandler = handler;
		this.guiManager = manager;
		this.shoppingCart = cart;
		this.editingCustomer = null;
		this.returningRentalTransIDs = new ArrayList<Integer>();
	}

	/**
	 * Gets the guiManager.
	 * @return the guiManager
	 */
	public GUIManager getGuiManager() {
		return guiManager;
	}
	
	/**
	 * Gets the login handler.
	 * @return the login handler
	 */
	public LoginHandler getLoginHandler() {
		return this.loginHandler;
	}
	
	/**
	 * Gets the data source.
	 * @return the data source
	 */
	public QueryHandler getDatasource() {
		return this.datasource;
	}

	/**
	 * gets the shopping cart
	 * @return the shoppingCart
	 */
	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

	/**
	 * Sets the shopping cart.
	 * @param shoppingCart the shoppingCart to set
	 * 
	 * @postcondition this.shoppingCart == shoppingCart
	 */
	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}
	
	/**
	 * gets the customer being edited
	 * @return the customer being edited
	 */
	public Customer getEditingCustomer() {
		return this.editingCustomer;
	}
	
	/**
	 * sets the customer being edited.
	 * @param cust the customer to be edited.
	 * 
	 * @postcondition this.editingCustomer == cust
	 */
	public void setEditingCustomer(Customer cust) {
		this.editingCustomer = cust;
	}

	/**
	 * gets the rentalReturnIDs
	 * 
	 * @return the IDs of the rentals being returned
	 */
	public List<Integer> getRentalReturnID() {
		return this.returningRentalTransIDs;
	}
	
	/**
	 * sets the rentalReturnIDs
	 * 
	 * @param id the IDs of the rentals being returned
	 * @postcondition this.returningRentalTransIDs == id.
	 */
	public void setRentalReturnID(List<Integer> id) {
		this.returningRentalTransIDs = id;
	}
	
	/**
	 * adds an id to the rentalReturnIDs
	 * 
	 * @param id the ID to be added
	 * @postcondition this.returningRentalTransIDs += id
	 */
	public void addRentalReturnID(int id) {
		this.returningRentalTransIDs.add(id);
	}
		
}
