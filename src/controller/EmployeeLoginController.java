package controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Handles employee login
 * @author Justin
 *
 */
public class EmployeeLoginController {

	/**
	 * Checks to see if a string contains special characters.
	 * @param a string
	 * @return true if contains special chars, false otherwise.
	 */
	public boolean containsSpecialChars(String str) {
		String regex = "^[a-zA-Z0-9]+$";
		Pattern patt = Pattern.compile(regex);
		Matcher match = patt.matcher(str);
		return !match.matches();
	}

	/**
	 * Try to log in with supplied credentials.
	 * @param username the username to log in under
	 * @param password the password to log in with
	 */
	public boolean tryLogin(String username, String password) {
		return ApplicationRegistry.get().getLoginHandler().login(username, password);
	}
}
