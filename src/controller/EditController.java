package controller;

import model.Customer;
/**
 * Edits/Adds customers
 * @author Justin
 *
 */
public class EditController {

	/**
	 * Adds the specified customer to the database.
	 * @param customer the customer to be added
	 * @return whether the customer was successfully added
	 */
	public boolean addCustomer(Customer customer) {
		return ApplicationRegistry.get().getDatasource().getCustomerQuery().addCustomer(customer);
	}

	/**
	 * Edits the specified customer in the database
	 * @param customer the customer to be edited
	 * @return whether the customer was successfully edited
	 */
	public boolean editCustomer(Customer customer) {
		return ApplicationRegistry.get().getDatasource().getCustomerQuery().editCustomer(customer);
	}

}
