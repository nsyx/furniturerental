package controller;

import data.QueryHandler;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import model.LoginHandler;
import model.ShoppingCart;
import view.GUIManager;
/**
 * Handles main functionality for app
 * @author Justin
 *
 */
public class Main extends Application {

	@Override
	public void start(Stage primaryStage) {
		
		GUIManager guiManager = new GUIManager(primaryStage);
		QueryHandler database = new QueryHandler();
		LoginHandler handler = new LoginHandler(database);
		ShoppingCart shoppingCart = new ShoppingCart();
		ApplicationRegistry.initialize(guiManager, database, handler, shoppingCart);
		
		this.addHooks();
		try {
			BorderPane rootLayout = (BorderPane) ApplicationRegistry.get().getGuiManager().getRootLayout();
			rootLayout.setCenter(ApplicationRegistry.get().getGuiManager().getLoginView());
			rootLayout.setBottom(ApplicationRegistry.get().getGuiManager().getConnectionView());
			Scene scene = new Scene(rootLayout);
			primaryStage.setTitle("Information Management Application");
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Add a hook to shutdown any remaining connections on program exit.
	 */
	private void addHooks() {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				// Program exited, connection closed.
				if (ApplicationRegistry.get().getDatasource().getConnection() != null) {
					ApplicationRegistry.get().getDatasource().closeConnection();
				}
			}
		}));
	}
}
