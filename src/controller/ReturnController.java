package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import data.QueryHandler;
import data.TransactionQuery;
import javafx.collections.ObservableList;
import model.Customer;
import model.Employee;
import model.FurnitureCheckOuts;
import model.FurnitureReturns;
import model.LoginHandler;

/**
 * Handles furniture returns
 * @author Laura
 *
 */
public class ReturnController {
	
	private TransactionQuery transqueries;
	
	/**
	 * Creates a new return controller
	 * @postcondition this.transqueries = new TransactionQuery
	 */
	public ReturnController() {
		this.transqueries = new TransactionQuery();
	}

	/**
	 * Gets all items to be returned based off of the rental transaction ID
	 * @param rTransID the rental transaction ID to draw returns from
	 * @return all items to be returned based off of the rental transaction ID
	 */
	public List<FurnitureReturns> getReturnItems(int rTransID) {
		return this.transqueries.getFurnitureReturns(rTransID);
	}

	/**
	 * Returns the specified furniture items
	 * @param returns the furniture items to be returned
	 * @return whether the furniture was successfully returned or not
	 * @posctcondition the database is updated to reflect the returned furniture.
	 * @throws SQLException
	 */
	public boolean returnFurniture(List<FurnitureReturns> returns) throws SQLException {
		String uname = LoginHandler.getSessionToken().getUsername();
		
		int eid = this.getDataSource().getSessionQuery()
				.getEmployeeIDByUsername(uname);
		Employee employee = new Employee(eid, uname);
		
		if(!returns.isEmpty()) {
			for(FurnitureReturns i : returns) {
				i.setOverseeingEmployee(employee);
			}
			boolean success = true;
			for(int i = 0; i < ApplicationRegistry.get().getRentalReturnID().size(); i++) {
				int rentID = ApplicationRegistry.get().getRentalReturnID().get(i);
				List<FurnitureReturns> curReturns = new ArrayList<FurnitureReturns>();
				for(FurnitureReturns fr : returns) {
					if(fr.getRTID() == rentID) {
						curReturns.add(fr);
					}
				}
				if(!curReturns.isEmpty()) {
					if(!this.getDataSource().getTransactionQuery().returnItems(curReturns, rentID)) {
						success = false;
					}
				}
				
			}
			return success;
		} else {
			return false;
		}
	}

	private QueryHandler getDataSource() {
		return ApplicationRegistry.get().getDatasource();
	}
	
	
}
