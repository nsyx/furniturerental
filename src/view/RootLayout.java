package view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class RootLayout {
	
	@FXML private Label label;
	
	/**
     * Initializes the instance of the main page
     * @precondition none
     * @postcondition the page is initialized 
     */
    @FXML
    void initialize() {

    	
    }

}
