package view.components;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;

import com.mysql.cj.exceptions.CJCommunicationsException;

import controller.ApplicationRegistry;
import javafx.fxml.FXML;
import model.LoginHandler;
import view.GUIManager;
import view.Page;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;

/**
 * A class for controlling the connection info bar.
 */
public class InfoPane extends Page {
	@FXML private ImageView connectedImg;
	@FXML private Label loginLabel;
	@FXML private Button logoutButton;
	@FXML private FlowPane userPane;
	
	@FXML
	public void initialize() {
		updateConnected();
	}
	
	/**
	 * Alters the labels to reflect the application's current connection and login state.
	 * @param isConnected
	 * @throws CJCommunicationsException 
	 * @throws ConnectException 
	 */
	public void updateConnected() {
		
		if(LoginHandler.getLoggedIn()) {
			try {
				
				connectedHelper(ApplicationRegistry.get().getDatasource()
						.getSessionQuery().getCurrentUser(), "img/username_icon.png", true);
			} catch (NullPointerException e) {
				loginLabel.setText("");
				logoutButton.setVisible(false);
				logoutButton.setManaged(false);
			}
 		} else {
			connectedHelper("", "img/Network-Disconnected-icon2.png", false);
		}
	}
	
	public void logoutClicked() {
		try {
			logout();
		} catch(IOException e) {
			showErrorWindow("We're sorry, but a critical error has occured", "Critical error");
		}
	}
	
	
	private void logout() throws IOException {
		ApplicationRegistry.get().getLoginHandler().logout();
		this.moveToDifferentPage(GUIManager.loginViewPath);
	}
	
	/**
	 * A helper method for updateConnected()
	 * @param text
	 * @param text2
	 * @param file
	 */
	private void connectedHelper(String text2, String file, boolean visible) {
		
		loginLabel.setVisible(visible);
		loginLabel.setText(text2);
		logoutButton.setVisible(visible);
		logoutButton.setManaged(visible);
		
		FileInputStream input;
		try {
			input = new FileInputStream(file);
			Image image = new Image(input);
			connectedImg.setImage(image);
			
			if(visible) {
				connectedImg.setFitHeight(30);
				connectedImg.setFitWidth(30);
			} else {
				connectedImg.setFitHeight(60);
				connectedImg.setFitWidth(60);
			}
			
		} catch (FileNotFoundException e) {
			showCriticalErrorWindow();
		}
	}
}
