package view;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GUIManager {
	
	public static String rootLayoutViewPath 		= "../view/RootLayout.fxml";
	public static String registrationViewPath 		= "../view/Registration.fxml";
    public static String loginViewPath 				= "../view/EmployeeLogin.fxml";
    public static String homeViewPath 				= "../view/HomePane.fxml";
    public static String connectionViewPath 		= "../view/components/InfoPane.fxml";
    public static String searchViewPath 			= "../view/SearchPane.fxml";
    public static String checkoutViewPath 			= "../view/Checkout.fxml";
    public static String returnViewPath 			= "../view/Return.fxml";
    
    private Stage applicationStage;
    private Pane loadedRootLayout;
    private Pane loadedRegistrationView;
    private Pane loadedLoginView;
    private Pane loadedHomeView;
    private Pane loadedConnectionView;
    private Pane loadedSearchView;
    private Pane loadedCheckoutView;
    private Pane loadedReturnView;
    
    public GUIManager(Stage applicationStage) {
    	this.applicationStage = applicationStage;
    	this.preloadGUIComponents();
    }
    
    private void preloadGUIComponents() {
    	try {
    		this.loadedRootLayout               = this.loadPane(GUIManager.rootLayoutViewPath);
    		this.loadedRegistrationView         = this.loadPane(GUIManager.registrationViewPath);
    		this.loadedLoginView                = this.loadPane(GUIManager.loginViewPath);
    		this.loadedSearchView               = this.loadPane(GUIManager.searchViewPath);
    		reloadInfo();
    		this.loadedCheckoutView 			= this.loadPane(GUIManager.checkoutViewPath);
    		this.loadedReturnView               = this.loadPane(GUIManager.returnViewPath);
    	}
    	catch (IOException e) {
    		System.err.println("Failed to load gui components. IO Error");
			e.printStackTrace();
    	}
    }
    
    public void reloadInfo() {
    	try {
			this.loadedConnectionView = this.loadPane(GUIManager.connectionViewPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public Pane loadPane(String path) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(GUIManager.class.getResource(path));
		Pane pane = (Pane) loader.load();
		return pane;
	}

	public Pane getRegistrationView() {
		return this.loadedRegistrationView;
	}
	
	public Pane getCheckoutView() {
		return this.loadedCheckoutView;
	}
	
	public Pane getReturnView() {
		return this.loadedReturnView;
	}

	public Pane getLoginView() {
		return this.loadedLoginView;
	}

	public Pane getHomeView() {
		return this.loadedHomeView;
	}

	public Pane getConnectionView() {
		return this.loadedConnectionView;
	}

	public Pane getSearchView() {
		return this.loadedSearchView;
	}

	public Pane getRootLayout() {
		return this.loadedRootLayout;
	}
	
	public Stage getApplicationStage() {
		return this.applicationStage;
	}
}
