package view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import controller.ApplicationRegistry;
import controller.ReturnController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import model.FurnitureReturns;

public class ReturnPage extends Page {

	@FXML
    private Button backButton;

    @FXML
    private Label customerLabel;

    @FXML
    private Button returnButton;

    @FXML
    private Spinner<Integer> qtySpinner;

    @FXML
    private Button removeItemButton;

    @FXML
    private Label totalLabel;

    @FXML
    private Label dayDueDisplay;

    @FXML
    private TableView<FurnitureReturns> tableView;
    
    private ReturnController controller;
	private ObservableList<FurnitureReturns> tableData;
	private double totalFees;

    @FXML
	public void initialize() {
		if (ApplicationRegistry.hasInitialized()) {
			controller = new ReturnController();
			tableData = FXCollections.observableArrayList();
			IntegerSpinnerValueFactory qtyFactory = new IntegerSpinnerValueFactory(1, 9999, 1, 1);
			qtySpinner.setValueFactory(qtyFactory);
			qtySpinner.valueProperty().addListener((obs, oldValue, newValue) -> qtySpinnerChanged());
			setTableListener();
//			setDateListener();
			populateTable();
			tableView.setItems(tableData);
			if(!tableData.isEmpty()) {
				updateTotalFees();
				dayDueDisplay.setText(this.tableData.get(0).getDueDate().toString());
				setReturnAbility();
			}
		}
	}

    private void setReturnAbility() {
		this.returnButton.setDisable(true);
		for(FurnitureReturns fr : this.tableData) {
			if(fr.getMaxQuantity() > fr.getReturnedQuantity()) {
				this.returnButton.setDisable(false);
			}
		}
	}

	private void qtySpinnerChanged() {
    	FurnitureReturns item = tableView.getSelectionModel().getSelectedItem();
		item.setQuantity(qtySpinner.getValueFactory().getValue());
		updateTotalFees();
		((TableColumn<?, ?>) tableView.getColumns().get(0)).setVisible(false);
		((TableColumn<?, ?>) tableView.getColumns().get(0)).setVisible(true);
	}

	private void populateTable() {
		//TableColumn rentTransID = new TableColumn<FurnitureCheckOuts, String>("Rental Transaction ID");
		TableColumn fidColumn = new TableColumn<FurnitureReturns, Integer>("FID");
		//TableColumn descriptionColumn = new TableColumn<FurnitureCheckOuts, String>("Description");
		TableColumn categoryColumn = new TableColumn<FurnitureReturns, String>("Type");
		TableColumn styleColumn = new TableColumn<FurnitureReturns, String>("Style");
		TableColumn lateRateColumn = new TableColumn<FurnitureReturns, String>("DailyLateFee");
		TableColumn lateFeeColumn = new TableColumn<FurnitureReturns, String>("ItemLateFees");
		TableColumn quantityColumn = new TableColumn<FurnitureReturns, Integer>("QTYReturning");
		TableColumn returnedColumn = new TableColumn<FurnitureReturns, Integer>("QTYReturned");
		
		fidColumn.setCellValueFactory(new PropertyValueFactory("fid"));
		categoryColumn.setCellValueFactory(new PropertyValueFactory("type"));
		styleColumn.setCellValueFactory(new PropertyValueFactory("style"));
		lateRateColumn.setCellValueFactory(new PropertyValueFactory("lateRate"));
		lateFeeColumn.setCellValueFactory(new PropertyValueFactory("lateFees"));
		quantityColumn.setCellValueFactory(new PropertyValueFactory("quantity"));
		returnedColumn.setCellValueFactory(new PropertyValueFactory("returnedQuantity"));
		
		//Furniture f = new Furniture(0, 0, "type", "style", "", 2.0, 3.0);
		//FurnitureReturns test = new FurnitureReturns(f, new Timestamp(System.currentTimeMillis()), new Date(System.currentTimeMillis()), 1, 0);

		tableView.getColumns().addAll(fidColumn, categoryColumn, styleColumn, lateRateColumn, lateFeeColumn, quantityColumn, returnedColumn);
		tableView.setItems(tableData);
		//ArrayList<FurnitureReturns> test2 = new ArrayList<>();
		//test2.add(test);
		List<FurnitureReturns> td = new ArrayList<FurnitureReturns>();
		for(int i = 0; i < ApplicationRegistry.get().getRentalReturnID().size(); i++) {
			List<FurnitureReturns> partList = this.controller.getReturnItems(ApplicationRegistry.get().getRentalReturnID().get(i));
			if(partList != null) {
				td.addAll(partList);
			}
		}
		if(td != null) {
			tableData.addAll(td);
		}
	}

	@FXML
    void backButtonClicked(ActionEvent event) {
    	try {
			this.moveToDifferentPage(GUIManager.searchViewPath);
		} catch (IOException e) {
			showErrorWindow("Sorry, something went wrong", "Critial Application Error");
		}
    	ApplicationRegistry.get().setRentalReturnID(new ArrayList<Integer>());
    }

    @FXML
    void removeItemClicked(ActionEvent event) {
    	FurnitureReturns item = tableView.getSelectionModel().getSelectedItem();
		tableView.getItems().remove(item);
		updateRemoveButton();
		updateTotalFees();
		setReturnAbility();
    }

    @FXML
    void returnClicked(ActionEvent event) {
    	boolean success = false;
		try {
			success = this.controller.returnFurniture(this.tableData);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		if (!success) {
			String msg = "Check to make sure that all items to be returned were rented";
			showErrorWindow(msg, "Checkout Error");
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Success");
			alert.setHeaderText(null);
			alert.setContentText("Return was successful");
			Optional<ButtonType> result = alert.showAndWait();
	    	ApplicationRegistry.get().setRentalReturnID(new ArrayList<Integer>());
			if (result.get() == ButtonType.OK) {
				try {
					this.moveToDifferentPage(GUIManager.searchViewPath);
				} catch (IOException e) {
					showErrorWindow("Sorry, something went wrong", "Critial Application Error");
				}
			}
		}
    }
    
    private void setTableListener() {
		tableView.getSelectionModel().selectedItemProperty()
		.addListener((obs, oldSelection, newSelection) -> {
			tableSelectionChanged();
		});
	}
    
    public ReturnController getController() {
		return controller;
	}
    
    private void tableSelectionChanged() {
		boolean isNothingSelected = tableView.getSelectionModel().isEmpty();
		updateRemoveButton();
		qtySpinner.setDisable(isNothingSelected);

		if (!isNothingSelected) {
			FurnitureReturns item = tableView.getSelectionModel().getSelectedItem();
			int minQ = 1;
			if(item.getReturnedQuantity() > minQ) {
				minQ = item.getReturnedQuantity();
			}
			qtySpinner.setValueFactory(new IntegerSpinnerValueFactory(minQ, item.getMaxQuantity(), item.getQuantity(), 1));
		}
	}
    
    public void updateTotalFees() {
//		LocalDate date = dayPicker.getValue();
//		LocalDate today = LocalDate.now();
//		long days = today.until(date, ChronoUnit.DAYS);
		
    	//TODO: add code to calculate late fees
		//double total = controller.getCartTotal() * days;
    	double fees = 0;
    	for(FurnitureReturns fr : this.tableData) {
    		fees += fr.getLateFees();
    	}
		String result = String.format("%.2f", fees);
		totalLabel.setText("TOTAL FEES: $"+(result));
	}

	public void updateRemoveButton() {
		removeItemButton.setDisable(tableView.getSelectionModel().isEmpty());
	}
}
