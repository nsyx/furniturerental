package view;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import controller.ApplicationRegistry;
import controller.EditController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import model.Customer;

public class EditPage extends Page {
	private int namecharlimit = 50;
	
	@FXML Label titleLabel;
	@FXML private TextField fname;
	@FXML private TextField lname;
	@FXML private TextField phone1;
	@FXML private TextField phone2;
	@FXML private TextField phone3;
	@FXML private TextField email;
	@FXML private TextField street;
	@FXML private TextField city;
	@FXML private TextField apt;
	@FXML private ComboBox<String> state;
	private ObservableList<String> stateList;
	@FXML private TextField zip;
	@FXML private DatePicker dob;
	EditController controller;
	Customer editingCustomer;
	
	@FXML
	public void initialize() {
		controller = new EditController();
    	stateList = FXCollections.observableArrayList();
    	state.setItems(stateList);
    	populateStates();
    	if(ApplicationRegistry.hasInitialized()) {
    		if(ApplicationRegistry.get().getEditingCustomer() != null) {
    			editingCustomer = ApplicationRegistry.get().getEditingCustomer();
    			this.populateFields(editingCustomer);
    			titleLabel.setText("Edit Member");
    		} else {
    			titleLabel.setText("Register New Member");
    		}
    	}
    }

    private void populateFields(Customer cust) {
		this.fname.setText(cust.getFirstName());
		this.lname.setText(cust.getLastName());
		this.initializePhone(cust.getPhone());
		this.email.setText(cust.getEmail().replaceAll("NULL", ""));
		this.street.setText(cust.getAddress());
		this.city.setText(cust.getCity());
		this.apt.setText(cust.getApt().replaceAll("NULL", ""));
		this.initializeState(cust.getState());
		this.zip.setText(cust.getZip());
		this.dob.setValue(cust.getDob().toLocalDate());
	}

	private void initializeState(String state2) {
		this.state.setValue(state2);	
	}

	private void initializePhone(String phone) {
		this.phone1.setText(phone.subSequence(0, 3).toString());
		this.phone2.setText(phone.subSequence(3, 6).toString());
		this.phone3.setText(phone.subSequence(6, 10).toString());
	}

	/**
     * Show a dialogue confirming that the user wants
     * to cancel member registration.
     */
	@FXML
	public void cancelConfirm() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirm Cancel");
		alert.setContentText("Are you sure? Any information entered will be lost.");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			ApplicationRegistry.get().setEditingCustomer(null);
			try {
				this.moveToDifferentPage(GUIManager.searchViewPath);
			} catch (IOException e) {
				showErrorWindow("We're sorry, but something went wrong.", "Application Error");
			}
		}
	}

	/**
	 * Validate the input to all textfields
	 * before creating a Customer Object.
	 * @throws IOException 
	 */
	public void validateSubmission() throws IOException {
		
		// Grab values from controls
		String _fname = fname.getText();
		String _lname = lname.getText();
		String _phone = phone1.getText() + phone2.getText() + phone3.getText();
		String _email = email.getText(); 
		_email = (_email==null) ? "" : _email;
		String _street = street.getText();
		String _city = city.getText();
		String _apt = apt.getText(); 
		_apt = (_apt==null) ? "" : _apt;
		String _state = state.getValue();
		String _zip = zip.getText();
		String _dob = dob.getEditor().getText();
		
		// Check to see if inputs are valid
		boolean isFnameOnlyLetters = _fname.matches("[a-zA-Z]+");
		boolean isLnameOnlyLetters = _lname.matches("[a-zA-Z]+");
		boolean isPhoneOnlyNumbers = _phone.matches("[0-9]+");
		boolean isStreetOnlyAlphaNum = _street.matches("[a-zA-Z0-9 ]+");
		boolean isCityOnlyLetters = _city.matches("[a-zA-Z ]+");
		boolean isAptOnlyNumbers = _apt.matches("[0-9]+") | _apt.isEmpty();
		boolean isZipOnlyNumbers = _zip.matches("[0-9]+");
		Matcher email_matcher = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", 
				Pattern.CASE_INSENSITIVE).matcher(_email);

		if(		!_fname.isEmpty() &&
				!_lname.isEmpty() &&
				!_phone.isEmpty() &&
				!_street.isEmpty() &&
				!_city.isEmpty() &&
				!_zip.isEmpty() &&
				_dob.toString() != "") {
			
			// Validate input and throw error window if not, else confirm registration
			
			boolean error = false;
			String errorMessage = "";
			
			if(_fname.length() > namecharlimit | !isFnameOnlyLetters) {
				error = true;
				errorMessage = ("First Name is invalid. Make sure it contains only letters, no spaces, and does not exceed 50 characters.");
			} else if (_lname.length() > namecharlimit | !isLnameOnlyLetters) {
				error = true;
				errorMessage = ("Last Name is invalid. Make sure it contains only letters, no spaces, and does not exceed 50 characters.");
			} else if (phone1.getText().length() != 3 | phone2.getText().length() != 3 | phone3.getText().length() != 4 | !isPhoneOnlyNumbers) {
				error = true;
				errorMessage = ("Phone Number is invalid. Make sure it contains 10 numerical characters.");
			} else if (_street.length() > 50 | !isStreetOnlyAlphaNum) {
				error = true;
				errorMessage = ("Street is invalid. Make sure field only contains alphanumeric characters.");
			} else if (_city.length() > 50 | !isCityOnlyLetters) {
				error = true;
				errorMessage = ("City is invalid. Make sure the field only contains letters.");
			} else if (_apt.length() > 8 | !isAptOnlyNumbers) {
				error = true;
				errorMessage = ("Apt is invalid. Make sure the field only contains numbers.");
			} else if (_zip.length() !=5 | !isZipOnlyNumbers) {
				error = true;
				errorMessage = ("Zip is invalid. Make sure the field is a 5-digit number.");
			} else if (!email_matcher.find() && !_email.isEmpty()) {
				error = true;
				errorMessage = ("Invalid email.");
			} else {
				
				java.sql.Date sqldob;
				try {
					SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");
					java.util.Date date = sdf1.parse(dob.getEditor().getText());
					sqldob = new java.sql.Date(date.getTime());
					confirmRegistration(_fname, _lname, _phone, _email, _street, _city, _apt, _state, _zip, sqldob);
					ApplicationRegistry.get().setEditingCustomer(null);
					this.moveToDifferentPage(GUIManager.searchViewPath);
				} catch(ParseException e) {
					error = true;
					errorMessage = "Invalid date";
				}
			}
			if(error) {
				showErrorWindow(errorMessage, "Validation error");
			}
			
		} else {
			showErrorWindow("Please ensure that all required fields are filled in.", "Validation error");
		}
	}

	public void confirmRegistration(String firstname, String lastname, String phone, 
			String email, String address, String city, String apt, String state, 
			String zip, Date dob) throws IOException {
		
		Customer customer = null;
		
		if(editingCustomer == null) {
			customer = new Customer(firstname, lastname, phone, email, address, city, apt, state, zip, dob);
		} else {
			customer = new Customer(editingCustomer.getCid(),firstname, lastname, phone, email, address, city, apt, state, zip, dob);
		}
		
		boolean success = false;
		String message = "";
		
		if(ApplicationRegistry.get().getEditingCustomer() == null) {
			
			success = controller.addCustomer(customer);
			message = " has been registered successfully.";
		} else {
			success = controller.editCustomer(customer);
			message = " has been edited successfully.";
		}
		
		if(success) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Success");
			alert.setHeaderText(null);
			alert.setContentText(customer.getFirstName() + " " + customer.getLastName() + message);
			Optional<ButtonType> result = showAlertWindow(customer.getFirstName() +
					" " + customer.getLastName() + message, "Success");
			
			if (result.get() == ButtonType.OK) {
				ApplicationRegistry.get().setEditingCustomer(null);
				this.moveToDifferentPage(GUIManager.searchViewPath);
			}
		} else {
			showErrorWindow("We're sorry, but something went wrong", "Operation failed");
		}
	}
	
	private void populateStates() {
		state.getItems().addAll(
				"AL", "AZ", "AR", "AK", "CA", "CO", "CT", "DE", "FL", "GA",
				"HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
				"MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
				"NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
				"SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"
				);
	}
}
