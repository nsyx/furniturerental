package view;

import java.io.IOException;
import java.net.ConnectException;
import controller.EmployeeLoginController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;

public class EmployeeLoginPage extends Page {

	@FXML private ProgressIndicator progressIndicator;
	@FXML private ImageView padlockIcon;
	@FXML private Button employeeLoginButton;
	@FXML private TextField employeeUsernameTextField;
	@FXML private PasswordField employeePasswordField;
	@FXML private Label loginLabel;
	@FXML private Label errorLabel;
	@FXML private Button employeeRegisterButton;
	private EmployeeLoginController controller;
	private Border border;
	private boolean changePage;

	public EmployeeLoginPage() {
		this.controller = new EmployeeLoginController();
	}

	/**
	 * Starts a new thread that validates the input of the
	 * username and password fields before logging a user in.
	 */
	public void loginClicked() {

		Task<Void> loginTask = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				try {
					validateLogin();
				} catch (ConnectException e) {
					showError("Can't connect to database. Check network or VPN connection");
				} catch (Exception e) {
					showError("An unknown error occured while trying to login");
				}
				return null;
			}
		};
		loginTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				setLoginLocked(false);
				if(changePage) {
					moveToSearchPage();
				}
			}
		});
		new Thread(loginTask).start();
	}
	
	/**
	 * Try to move to the search page upon successful login.
	 */
	protected void moveToSearchPage() {
		try {
			moveToDifferentPage(GUIManager.searchViewPath);
		} catch (IOException e) {
			showCriticalErrorWindow();
		}
	}

	@FXML
	public void validateLogin() throws IOException {
		showError("");
		changePage = false;
		border = employeeUsernameTextField.getBorder();
		String username = this.employeeUsernameTextField.getText().trim();
		String password = this.employeePasswordField.getText().trim();
		employeePasswordField.clear();

		if (username.isEmpty() | password.isEmpty()) {
			setErrorBorders();
		} else if (this.controller.containsSpecialChars(username)) {
			showError("Usernames should be alphanumeric only");
		} else {
			setLoginLocked(true);
			try {
				boolean loginSuccess = controller.tryLogin(username, password);

				if (!loginSuccess) {
					showError("Wrong username or password");
				} else {
					changePage = true;
				}
			} catch (Exception e) {
				showError("Failed to connect to database. Check network or VPN connection");
			}
		}
	}
	
	/**
	 * Highlight text fields with a red border.
	 */
	@FXML
	public void setErrorBorders() {
		this.highlightTextField(employeeUsernameTextField);
		this.highlightTextField(employeePasswordField);
	}
	
	/**
	 * Handle the event when the login button
	 * is clicked.
	 * @param me
	 */
	@FXML
	public void onClick(MouseEvent me) {
		this.resetTextField(employeeUsernameTextField, border);
		this.resetTextField(employeePasswordField, border);
	}
	
	/**
	 * Handle the event when the user presses enter
	 * while inside a TextField.
	 * @param ke
	 * @throws IOException
	 */
	@FXML
	public void onEnter(KeyEvent ke) throws IOException {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			loginClicked();
		}
	}
	
	/**
	 * Starts a new thread that shows any error 
	 * messages below the login page.
	 * @param error
	 */
	@FXML
	private void showError(String error) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				errorLabel.setText(error);
			}
		});

	}
	
	/**
	 * Lock parts of the gui and show progress indicator.
	 * @param locked
	 */
	public void setLoginLocked(boolean locked) {
		padlockIcon.setVisible(!locked);
		progressIndicator.setVisible(locked);
		employeeLoginButton.setDisable(locked);
		employeeUsernameTextField.setDisable(locked);
		employeePasswordField.setDisable(locked);
	}

	/**
	 * @return the border
	 */
	public Border getBorder() {
		return border;
	}
}