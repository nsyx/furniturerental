package view;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import controller.ApplicationRegistry;
import controller.SearchPaneController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.Customer;
import model.Furniture;
import model.LoginHandler;

public class SearchPage extends Page {
	
	private static Stage registrationWindow;
	@FXML private Button editButton;
	@FXML private Button viewRentalsButton;
	@FXML private Button viewReturnsButton;
	@FXML private Button searchButton;
	@FXML private Button queryBoxButton;
	@FXML private Button execButton;
	@FXML private Button loadCustomerProfileButton;
	@FXML private Button returnRentalsButton;
	@FXML private Button clearButton;
	@FXML private Button viewItemHistoryButton;
	@FXML private ScrollPane queryBox;
	@FXML private TextArea queryArea;
	@FXML private Button searchClearButton;
	@FXML private BorderPane adminPanel;
	@FXML private FlowPane customerSearches;
	@FXML private FlowPane furnitureSearches;
	@FXML private Button cartButton;
	@FXML private Button addToCartButton;
	@FXML private Label cartLabel;
	@FXML private Label messageLabel;
	@FXML private Label loadedCustomerLabel;
	@FXML private Label selectedItemLabel;
	@FXML private Label itemAddedLabel;
	@FXML private TableView<Object> tableView;
	private ObservableList<Object> tableData;
	@FXML private ComboBox<String> tableCriteria;
	private ObservableList<String> tableCriteriaList;
	private SearchPaneController controller;
	@FXML MenuItem newCustomer;
	@FXML MenuItem logout;
	@FXML private TextField cidField;
	@FXML private TextField fnameField;
	@FXML private TextField lnameField;
	@FXML private TextField phoneField;
	@FXML private TextField fidField;
	@FXML private TextField styleField;
	@FXML private TextField categoryField;
	@FXML private TextField descriptionField;
	Furniture selectedFurniture;
	Customer selectedCustomer;
	
	@FXML
	public void initialize() {
		if (ApplicationRegistry.hasInitialized()) {
			controller = new SearchPaneController();
			tableCriteriaList = FXCollections.observableArrayList();
			tableData = FXCollections.observableArrayList();
			tableCriteria.setItems(tableCriteriaList);
			tableView.setItems(tableData);
			
			tableView.getSelectionModel().selectedItemProperty()
			.addListener((obs, oldSelection, newSelection) -> {
			       tableSelectionChanged();
			});
			
			this.setAdminPanelVisible();
			selectedFurniture = null;
			selectedCustomer = null;
			populateComboBoxes();
			queryButtonClicked();
			clearButtonClicked();
			updateCustomerLoadedLabel();
		}
	}
	
	@FXML
	public void showEmployeeRegistrationWindow(ActionEvent event) {
		SearchPage.registrationWindow.show();
	}
	
	/**
	 * Shows the registration window.
	 * @throws IOException 
	 */
	public void showRegistrationWindow() throws IOException {
		this.moveToDifferentPage(GUIManager.registrationViewPath);
	}
	
	/**
	 * Shows the edit customer window.
	 * @throws IOException 
	 */
	 @FXML
	 void showEditWindow(ActionEvent event) throws IOException {
		try {
			Customer selected = constructCustomerFromTableData((ObservableList<?>) tableView.getSelectionModel().getSelectedItem());
			this.controller.setEditingCustomer(selected);
		 this.moveToDifferentPage(GUIManager.registrationViewPath);
		} catch (Exception e) {
			showErrorWindow("Not a valid customer", "Error");
		}
	 }
	
	public void logout() throws IOException {
		ApplicationRegistry.get().getLoginHandler().logout();
		this.moveToDifferentPage(GUIManager.loginViewPath);
	}

	/**
	 * Set the admin panel visible is the current session is administrator.
	 */
	public void setAdminPanelVisible() {
		boolean admin = LoginHandler.getSessionToken().isAdmin();
		if(!admin) {
			adminPanel.setManaged(false);
			adminPanel.setVisible(false);
		} else {
			adminPanel.setManaged(true);
			adminPanel.setVisible(true);
		}
	}
	
	private void populateComboBoxes() {
		tableCriteria.getItems().add("Customer");
		tableCriteria.getItems().add("Furniture");
		tableCriteria.getSelectionModel().selectFirst();
		tableCriteriaChanged();
	}
	
	public void tableCriteriaChanged() {
		String selectedTable = tableCriteria.getSelectionModel().getSelectedItem();
		
		if(selectedTable.equals("Customer")) {
			customerChosen();
		} else if(selectedTable.equals("Furniture")) {
			furnitureChosen();
		}
	}
	
	private void customerChosen() {
		furnitureSearches.setManaged(false);
		furnitureSearches.setVisible(false);
		customerSearches.setManaged(true);
		customerSearches.setVisible(true);
	}
	
	private void furnitureChosen() {
		furnitureSearches.setManaged(true);
		furnitureSearches.setVisible(true);
		customerSearches.setManaged(false);
		customerSearches.setVisible(false);
	}

	public SearchPaneController getController() {
		return controller;
	}
	
	private void clearTable() {
		tableView.getColumns().clear();
		tableData.clear();
		tableSelectionChanged();
	}
	
	public void searchClicked() {
		clearTable();
		populateTableFromSearch();
	}
	
	public void viewFurnitureHistoryClicked() {
		Furniture f = getSelectedFurniture();
		try {
			ResultSet rs = controller.loadFurnitureRentalHistory(f.getIid());
			clearTable();
			controller.buildTableFromResults(rs, tableData, tableView);
		} catch (SQLException e) {
			showErrorWindow("Invalid item", "Error");
		}
	}
	
	public void populateTableFromSearch() {
		try {
			String selectedTable = tableCriteria.getSelectionModel().getSelectedItem();
			ResultSet rs = null;
			tableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			
			if(selectedTable.equals("Customer")) {
				rs = controller.execCustomerSearch(getCustomerSearchFields());
			} else if(selectedTable.equals("Furniture")) {
				rs = controller.execFurnitureSearch(getFurnitureSearchFields());
			}
			controller.buildTableFromResults(rs, tableData, tableView);
		} catch (SQLException e) {
			showErrorWindow(e.getMessage(), "SQL error");
		} catch (IllegalArgumentException e) {
			messageLabel.setText("Failed to run last command due to illegal argument");
			showErrorWindow(e.getMessage(), "Illegal argument");
		}
	}
	
	
	public String[] getCustomerSearchFields() {

		String cid = cidField.getText();
		String fname = fnameField.getText();
		String lname = lnameField.getText();
		String phone = phoneField.getText();
		
		String fields[] = new String[] {
				cid,
				fname,
				lname,
				phone
		};	
		return fields;
	}
	
	public String[] getFurnitureSearchFields() {
		
		String fid = fidField.getText();
		String style = styleField.getText();
		String category = categoryField.getText();
		String description = descriptionField.getText();
		
		String fields[] = new String[] {
				fid,
				style,
				category,
				description
		};	
		return fields;
	}
	
	public void loadCustomerProfileClicked() {
		Customer s = getSelectedCustomer();
		if(s != null) {
			controller.beginCustomerTransaction(s);
			updateCustomerLoadedLabel();
		} else {
			showErrorWindow("Not a valid customer", "Error");
		}
	}
	
	private void updateCustomerLoadedLabel() {
		try {
			Customer customer = controller.getLoadedCustomer();
			loadedCustomerLabel.setText(customer.getFirstName() + " " + customer.getLastName() + "\n" + "MemberID: #" + customer.getCid());
		} catch (NullPointerException e) {
			loadedCustomerLabel.setText("(No Customer Loaded)");
		}
	}
	
	private Customer constructCustomerFromTableData(ObservableList<?> object) throws ParseException {
		return controller.constructCustomerFromTableData(object);
	}
	
	private Furniture constructFurnitureFromTableData(ObservableList<?> object) throws ParseException {
		return controller.constructFurnitureFromTableData(object);
	}

	public void cartButtonClicked() {
		try {
			this.moveToDifferentPage(GUIManager.checkoutViewPath);
		} catch (IOException e) {
			showErrorWindow("A critial error has occured. "
					+ "A JavaFX component failed to load", "Critial application error");
		} catch (Exception e) {
			showErrorWindow("Not a valid customer", "Error");
		}
	}

	public void tableSelectionChanged() {
		editButton.setDisable(true);
		viewRentalsButton.setDisable(true);
		viewReturnsButton.setDisable(true);
		loadCustomerProfileButton.setDisable(true);
		addToCartButton.setDisable(true);
		returnRentalsButton.setDisable(true);
		viewItemHistoryButton.setDisable(true);
		this.highlightLabel(selectedItemLabel, Color.WHITE);
		selectedItemLabel.setText("(No Item Selected)");
		
		try {
			String firstColumnName = tableView.getColumns().get(0).getText();
			if (firstColumnName.contentEquals("fid")) {
				furnitureTableSelected();
			} else if (firstColumnName.contentEquals("cid")) {
				customerTableSelected();
			} else if (firstColumnName.contentEquals("rentalTransactionID")) {
				rentalHistoryTableSelected();			
			}
		} catch(Exception e) {
			
		}
		updateCustomerLoadedLabel();
		updateFurnitureSelectedLabel();
	}

	private void rentalHistoryTableSelected() {
		
        ObservableList<Object> selectedItems = tableView.getSelectionModel().getSelectedItems();
        List<Integer> ids = new ArrayList<Integer>();
        for(Object item : selectedItems) {
    		String rental = item.toString();
    		System.out.println(rental);
    		int rentalID = Integer.parseInt(rental.subSequence(1, 3).toString());
    		ids.add(rentalID);
        }
		
		this.controller.setRentalReturnID(ids);
		returnRentalsButton.setDisable(false);		
	}

	public void updateFurnitureSelectedLabel() {
		Furniture furniture = getSelectedFurniture();
		int onHand = 0;
		if(furniture != null) {
			try {
				onHand = furniture.getOnHand();
			} catch (SQLException e) {
				showErrorWindow(e.getMessage(), "SQL Error");
			}
			if (onHand > 0) {
				this.highlightLabel(selectedItemLabel, Color.GREEN);
				addToCartButton.setDisable(false);
			} else {
				this.highlightLabel(selectedItemLabel, Color.RED);
			}
			selectedItemLabel.setText("# In Stock: " + onHand);
			viewItemHistoryButton.setDisable(false);
		}
	}
	
	public void furnitureTableSelected() throws ParseException {
		setSelectedCustomer(null);
		Furniture furniture = constructFurnitureFromTableData(
				(ObservableList<?>) tableView.getSelectionModel().getSelectedItem());
		setSelectedFurniture(furniture);
	}

	public void customerTableSelected() throws ParseException {
		setSelectedFurniture(null);
		Customer customer = constructCustomerFromTableData((ObservableList<?>) tableView.getSelectionModel().getSelectedItem());
		loadedCustomerLabel.setText(customer.getFirstName() + " " + customer.getLastName());
		setSelectedCustomer(customer);
		editButton.setDisable(false);
		viewRentalsButton.setDisable(false);
		viewReturnsButton.setDisable(false);
		loadCustomerProfileButton.setDisable(false);
	}

	public void setSelectedCustomer(Customer customer) {
		selectedCustomer = customer;
	}
	
	public Customer getSelectedCustomer() {
		return selectedCustomer;
	}
	
	public void setSelectedFurniture(Furniture furniture) {
		selectedFurniture = furniture;
	}
	
	public Furniture getSelectedFurniture() {
		return selectedFurniture;
	}
	
	public void addToCartClicked() {
		Furniture f = null;
		try {
			f = getSelectedFurniture();
			controller.addItemToCart(f);
			itemAddedLabel.setText("#" + f.getFid() + " added to cart");
		} catch(NullPointerException e) {
			showErrorWindow("Can't add non-furniture item to shopping cart", "Error");
		} catch(IllegalArgumentException e) {
			itemAddedLabel.setText("#" + f.getFid() + " already in cart");
		}
	}
	
	public void queryButtonClicked() {
		boolean visible = queryBox.isVisible();
		boolean managed = queryBox.isManaged();
		queryBox.setVisible(!visible);
		queryBox.setManaged(!managed);
		execButton.setVisible(!visible);
		clearButton.setVisible(!visible);
		messageLabel.setVisible(!visible);
		messageLabel.setManaged(!visible);
		
		if(queryBox.isVisible()) {
			queryBoxButton.setText("-");
		} else {
			queryBoxButton.setText("+");
		}
	}
	
	public void viewRentHistoryClicked() {
		try {
			Customer selected = constructCustomerFromTableData((ObservableList<?>) 
					tableView.getSelectionModel().getSelectedItem());
			ResultSet rs = controller.loadRentHistory(selected);
			clearTable();
			controller.buildTableFromResults(rs, tableData, tableView);
			tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		} catch (ParseException e) {
			showErrorWindow("Not a valid customer", "Error");
		} catch (SQLException e) {
			showErrorWindow("Failed to load table", "Error");
		}
	}
	
	public void viewReturnHistoryClicked() {
		try {
			Customer selected = constructCustomerFromTableData((ObservableList<?>)
					tableView.getSelectionModel().getSelectedItem());
			ResultSet rs = controller.loadReturnHistory(selected);
			clearTable();
			controller.buildTableFromResults(rs, tableData, tableView);
		} catch (ParseException e) {
			showErrorWindow("Not a valid customer", "Error");
		} catch (SQLException e) {
			showErrorWindow("Failed to load table", "Error");
		}
	}
	
	public void executeClicked() {
		execute();
	}
	
	public void clearButtonClicked() {
		queryArea.setText("");
		queryArea.selectEnd();
	}
	
	public void searchClearClicked() {
		cidField.setText("");
		fnameField.setText("");
		lnameField.setText("");
		phoneField.setText("");
		
		fidField.setText("");
		styleField.setText("");
		categoryField.setText("");
		descriptionField.setText("");
	}
	
	public void updateAdminMessage(String text) {
		try {
			String s = queryArea.getText();
			String r = s.substring(0, s.indexOf(" "));
			messageLabel.setText("> \'"+ r + "\' " + text);
		} catch (Exception e) {
			messageLabel.setText("> Command " + text);
		}
	}
	
	public void execute() {
		String text = queryArea.getText();
		clearTable();
		try {
			ResultSet rs = controller.executeAdminQuery(text);
			controller.buildTableFromResults(rs, tableData, tableView);
			updateAdminMessage("command executed successfully");
		} catch (SQLSyntaxErrorException e) {
			updateAdminMessage("failed to execute due to syntax error");
			showErrorWindow(e.getMessage(), "Syntax error");
		} catch (SQLException e) {
			updateAdminMessage("failed to execute due to SQL error");
			showErrorWindow(e.getMessage(), "SQL error");
		}
	}
	
    public void returnRentalClicked(ActionEvent event) {
		try {
			this.moveToDifferentPage(GUIManager.returnViewPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
