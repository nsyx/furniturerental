package view;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import controller.ApplicationRegistry;
import controller.CheckoutController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Alert.AlertType;
import model.Customer;
import model.FurnitureCheckOuts;
import model.InventoryItem;

public class CheckoutPage extends Page {
	@FXML Button checkoutBtn;
	@FXML Spinner<Integer> qtySpinner;
	@FXML Button backButton;
	@FXML Button removeItemButton;
	@FXML TableView<Object> tableView;
	@FXML Label customerLabel;
	@FXML Label totalLabel;
	@FXML DatePicker dayPicker;
	private ObservableList<Object> tableData;
	private CheckoutController controller;

	@FXML
	public void initialize() {
		if (ApplicationRegistry.hasInitialized()) {
			controller = new CheckoutController();
			tableData = FXCollections.observableArrayList();
			IntegerSpinnerValueFactory qtyFactory = new IntegerSpinnerValueFactory(1, 9999, 1, 1);
			qtySpinner.setValueFactory(qtyFactory);
			qtySpinner.valueProperty().addListener((obs, oldValue, newValue) -> qtySpinnerChanged());
			setTableListener();
			setDateListener();
			populateCart();
			tableView.setItems(tableData);
			updateTotal();
			updateCustomerButtons();
		}
	}
	
	public void updateCustomerButtons() {
		Customer c = controller.getCustomer();
		if((controller.getCustomer()==null)) {
			checkoutBtn.setDisable(true);
			customerLabel.setText("NO CUSTOMER");
		} else {
			checkoutBtn.setDisable(false);
			customerLabel.setText(c.getFirstName() + " " + c.getLastName());
		}
		
		if(controller.getAllFromCart().size()==0) {
			checkoutBtn.setDisable(true);
		}
	}
	
	/**
	 * Sets a listener on the table to call the change
	 * method when the table selection changes.
	 */
	private void setTableListener() {
		tableView.getSelectionModel().selectedItemProperty()
		.addListener((obs, oldSelection, newSelection) -> {
			tableSelectionChanged();
		});
	}
	
	/**
	 * Set a listener on the datePicker to disable all past dates
	 * and to update the checkout total when date is changed.
	 */
	private void setDateListener() {
		dayPicker.setValue(LocalDate.now().plusDays(30));
		dayPicker.setDayCellFactory(picker -> new DateCell() {
	        public void updateItem(LocalDate date, boolean empty) {
	            super.updateItem(date, empty);
	            LocalDate today = LocalDate.now().plusDays(1);
	            setDisable(empty || date.compareTo(today) < 0 );
	            Date dueDate = null;
	            try {
		            dueDate = Date.valueOf(dayPicker.getValue());
		            controller.updateDueDate(dueDate);
		            updateTotal();
	            	checkoutBtn.setDisable(false);

	            } catch (NullPointerException e) {
	            	checkoutBtn.setDisable(true);
	            }
	        }
	    });
	}

	public CheckoutController getController() {
		return controller;
	}
	
	/**
	 * Builds the columns for the TableView then calls the controller to
	 * add the items to the shopping cart.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void populateCart() {
		
		TableColumn fidColumn = new TableColumn<FurnitureCheckOuts, Integer>("fid");
		TableColumn descriptionColumn = new TableColumn<FurnitureCheckOuts, String>("description");
		TableColumn categoryColumn = new TableColumn<FurnitureCheckOuts, String>("category");
		TableColumn styleColumn = new TableColumn<FurnitureCheckOuts, String>("style");
		TableColumn dailyRateColumn = new TableColumn<FurnitureCheckOuts, Double>("dailyRate");
		TableColumn lateFeeColumn = new TableColumn<FurnitureCheckOuts, Double>("lateFee");
		TableColumn onHandColumn = new TableColumn<InventoryItem, Integer>("quantityInStock");
		TableColumn quantityColumn = new TableColumn<FurnitureCheckOuts, Integer>("quantityInCart");
		
		
		fidColumn.setCellValueFactory(new PropertyValueFactory("fid"));
		descriptionColumn.setCellValueFactory(new PropertyValueFactory("description"));
		categoryColumn.setCellValueFactory(new PropertyValueFactory("type"));
		styleColumn.setCellValueFactory(new PropertyValueFactory("style"));
		dailyRateColumn.setCellValueFactory(new PropertyValueFactory("dailyRate"));
		lateFeeColumn.setCellValueFactory(new PropertyValueFactory("lateFee"));
		onHandColumn.setCellValueFactory(new PropertyValueFactory("onHand"));
		quantityColumn.setCellValueFactory(new PropertyValueFactory("quantity"));
		
		tableView.getColumns().addAll(fidColumn, descriptionColumn, categoryColumn, styleColumn, dailyRateColumn, lateFeeColumn, quantityColumn, onHandColumn);
		tableView.setItems(tableData);
		tableData.addAll(controller.getAllFromCart());
	}
	
	/**
	 * Move back to the search page.
	 */
	public void backButtonClicked() {
		try {
			this.moveToDifferentPage(GUIManager.searchViewPath);
		} catch (IOException e) {
			showErrorWindow("Sorry, something went wrong", "Critial Application Error");
		}
	}
	
	/**
	 * Calls the controller to execute the SQL statements that will
	 * update the database with the transaction based on the currently
	 * loaded customer. If successful, move to a new page.
	 */
	public void checkoutClicked() {

		try {
			boolean success = this.controller.checkout();
			if (!success) {
				String msg = "Check to make sure that all items to be rented are in stock";
				showErrorWindow(msg, "Checkout Error");
			} else {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Success");
				alert.setHeaderText(null);
				alert.setContentText("Checkout was successful");
				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK) {
					try {
						postCheckout();
						this.moveToDifferentPage(GUIManager.searchViewPath);
					} catch (IOException e) {
						showErrorWindow("Sorry, something went wrong", "Critial Application Error");
					}
				}
			}

		} catch (SQLException e) {
			showErrorWindow("Unpecified error", "Checkout error");
		}
	}
	
	/**
	 * Removes the selected item from the cart.
	 */
	public void removeItemClicked() {
		FurnitureCheckOuts item = (FurnitureCheckOuts) tableView.getSelectionModel().getSelectedItem();
		controller.removeCartItem(item);
		tableView.getItems().remove(item);
		updateRemoveButton();
		updateTotal();
	}
	
	/**
	 * Clears the cart after checkout.
	 */
	public void postCheckout() {
		controller.clearCart();
	}
	
	/**
	 * Updates the view when the TableView selection changes.
	 */
	private void tableSelectionChanged() {
		boolean isNothingSelected = tableView.getSelectionModel().isEmpty();
		updateRemoveButton();
		qtySpinner.setDisable(isNothingSelected);

		if (!isNothingSelected) {
			FurnitureCheckOuts item = (FurnitureCheckOuts) tableView.getSelectionModel().getSelectedItem();
			qtySpinner.getValueFactory().setValue(item.getQuantity());
		}
	}
	
	/**
	 * Prints out the rounded transaction total
	 * on the text label.
	 */
	public void updateTotal() {
		LocalDate date = dayPicker.getValue();
		LocalDate today = LocalDate.now();
		long days = today.until(date, ChronoUnit.DAYS);
		
		double total = controller.getCartTotal() * days;
		String result = String.format("%.2f", total);
		totalLabel.setText("TOTAL: $"+(result));
	}

	public void updateRemoveButton() {
		removeItemButton.setDisable(tableView.getSelectionModel().isEmpty());
	}
	
	/**
	 * When the quantity spinner is changed, the the currently selected
	 * item is updated with the new value and the total cost is updated.
	 */
	public void qtySpinnerChanged() {
		FurnitureCheckOuts item = (FurnitureCheckOuts) tableView.getSelectionModel().getSelectedItem();
		item.setQuantity(qtySpinner.getValueFactory().getValue());
		updateTotal();
		// The next two lines are a workaround 
		// for a bug where the TableView won't properly update
		((TableColumn<?, ?>) tableView.getColumns().get(0)).setVisible(false);
		((TableColumn<?, ?>) tableView.getColumns().get(0)).setVisible(true);
	}
}
