-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 160.10.25.16:3306
-- Generation Time: Dec 01, 2019 at 10:32 PM
-- Server version: 5.7.27-log
-- PHP Version: 7.2.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs3230f19a`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `name` varchar(20) NOT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`name`, `description`) VALUES
('bed', 'King, queen, and twin sizes'),
('bookshelf', 'For storing knowledge'),
('chair', 'Kitchen chairs and stools'),
('desk', 'Small home office to business'),
('lamp', 'Various lighting for all needs'),
('nightstand', 'A table with drawers to go near your bed'),
('sofa', 'Large living room sofas'),
('table', 'Holds stuff');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `cid` int(4) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `phone` char(10) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `street` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `apt` varchar(10) DEFAULT NULL,
  `state` char(2) NOT NULL,
  `zip` char(5) NOT NULL,
  `dob` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cid`, `fname`, `lname`, `phone`, `email`, `street`, `city`, `apt`, `state`, `zip`, `dob`) VALUES
(7, 'Spam', 'Guy', '9987760000', 'yeah@ok.com', '18 goodbury ln', 'okay', '55', 'AR', '15453', '1989-10-22'),
(9, 'Bob', 'Harper', '9999999999', 'bharper@example.com', '8 willow brook dr', 'franklin', NULL, 'GA', '30118', '2019-10-22'),
(10, 'Johnna', 'Harper', '2349994567', 'jharper@example.com', '45 terry dr', 'carrollton', '33', 'CT', '67885', '1978-10-22'),
(13, 'Jalen', 'Smith', '1234567890', 'bb@example.com', '2345 fgd rd', 'erwe', '301', 'AK', '12345', '1996-09-30'),
(14, 'test', 'person', '2222222222', NULL, '54 main st', 'franklin', NULL, 'GA', '49121', '1988-11-01'),
(15, 'Ewan', 'Petersen', '6781876539', 'epeters2@nope.edu', '444 Jacklyn', 'Carrollton', '1505', 'GA', '30117', '2019-10-30'),
(16, 'test', 'test', '5555255555', 'testguy@example.com', '19 river st', 'Carrollton', '898', 'GA', '78978', '2019-10-15'),
(17, 'test', 'test', '4564564567', NULL, 'dsaf street', 'fuamcye', NULL, 'CT', '30118', '1977-10-11'),
(18, 'justin', 'mconnell', '5555555555', 'example@example.com', '1601 maple st', 'carrollton', NULL, 'GA', '30118', '2019-10-14'),
(19, 'Miles', 'Thatcher', '4563451243', 'mthatcher@example.com', '209 maple dr', 'newton', NULL, 'TX', '58292', '1978-11-07'),
(20, 'Terry', 'Brooks', '7784322235', 'tbrooks@example.com', '56 ferry smith ln', 'Atlanta', NULL, 'GA', '35567', '1989-03-05'),
(21, 'Bob', 'Sanchez', '4044044044', 'zanches@example.com', '123 fleet street', 'Greenville', '21', 'AZ', '40440', '1983-11-19'),
(22, 'Hank', 'Hill', '5554442543', 'hhill@example.com', '56 glen way', 'dallas', '102', 'TX', '90118', '1976-11-13'),
(23, 'Jason', 'Thatcher', '9993451235', 'jthatcher@example.com', '24 woodbridge rd', 'yeet', NULL, 'CT', '15567', '2019-11-12'),
(24, 'Shawn', 'Valentine', '6666666666', 'sd2013@yahoo.com', '77 river view ln', 'Hoity Toity Ville', '109', 'CA', '10287', '1988-11-18'),
(25, 'Joe', 'Shmoe', '7705555555', 'jsmoe@example.com', '45 woodbridge way', 'iirelgva', '678', 'UT', '38855', '1988-11-05'),
(26, 'Jill', 'Shmoe', '4452231234', 'adkskfj@example.com', '910 something road', 'nowhere', NULL, 'NH', '98281', '1999-11-05'),
(27, 'Shmoe', 'Shmoe', '2112223345', 'aijfiuy@foo.com', 'foo', 'bar', NULL, 'MO', '35576', '1966-11-03'),
(28, 'Guy', 'Smiley', '4448974532', NULL, 'gsmiley@foo.com', 'yolo', NULL, 'TX', '88765', '1965-10-10'),
(29, 'Karen', 'Tesla', '9786652213', 'hey@hey.com', '94 blue st', 'new york', NULL, 'NY', '21009', '1998-04-15'),
(30, 'Fred', 'Harper', '6789992234', 'fharper@example.com', '44 terrybridge dr', 'lmzyrnc', NULL, 'ND', '10846', '1988-10-14'),
(31, 'Chase', 'Ugillug', '5555555555', 'example@example.com', '88 yeah rd', 'Mapleton', '811', 'NH', '30778', '1977-11-13'),
(32, 'Spam', 'McSpam', '5555555555', 'spamspam@spam.com', '11 spam street', 'spam', '445', 'KS', '67899', '2000-11-13'),
(33, 'Harry', 'Jones', '6784444444', 'something@something.com', '2 yeet street', 'spam', NULL, 'ME', '28394', '1997-11-20');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `eID` int(11) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `bdate` date NOT NULL,
  `phone` char(10) NOT NULL,
  `address` varchar(50) NOT NULL,
  `gender` char(1) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address2` varchar(50) NOT NULL,
  `city` varchar(10) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip` int(11) DEFAULT NULL,
  `username` varchar(16) DEFAULT NULL,
  `isAdmin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`eID`, `fname`, `lname`, `bdate`, `phone`, `address`, `gender`, `email`, `address2`, `city`, `state`, `zip`, `username`, `isAdmin`) VALUES
(1, 'Ted', 'Baker', '1982-10-24', '2348884321', '78 willow st', 'M', 'tbaker1@example.com', '17 wodw dr', 'houston', 'TX', 74432, 'tbaker1', 0),
(2, 'Karen', 'Smith', '1989-08-07', '3247775454', '21 cedar st', 'F', NULL, '77 riverview lane', 'houston', 'TX', 55678, 'administrator', 1),
(3, 'Fred', 'Johnson', '1977-04-08', '6784453234', '567 main st', 'M', 'fjohnson81@rentme.com', '', 'new york', 'NY', 34345, 'fjohnson3', 0),
(4, 'Lisa', 'Sanchez', '1968-07-17', '7782342211', '98 hwy 35', 'F', 'lsanchez68@example.com', '', 'new york', 'NY', 78776, 'lsanchez1', 0),
(5, 'John', 'Reed', '1981-12-11', '3453349981', '64 hwy 29', 'M', 'jreed9@example.com', '', 'new york', 'NY', 23456, 'jreed2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `furniture`
--

CREATE TABLE `furniture` (
  `fid` int(11) NOT NULL,
  `category` varchar(20) NOT NULL,
  `style` varchar(20) NOT NULL,
  `description` varchar(50) NOT NULL,
  `dailyRate` decimal(15,2) NOT NULL,
  `lateFee` decimal(15,2) NOT NULL,
  `inventoryID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `furniture`
--

INSERT INTO `furniture` (`fid`, `category`, `style`, `description`, `dailyRate`, `lateFee`, `inventoryID`) VALUES
(3, 'sofa', 'rustic', 'CountryHomes 3-seat black', '1.52', '0.25', 1),
(4, 'chair', 'modern', 'ModernKitchen Barstool Silver', '0.75', '0.25', 2),
(5, 'sofa', 'modern', 'Chesterfield velvet sofa', '1.23', '0.30', 3),
(6, 'sofa', 'traditional', 'Zahra 5-piece fabric sectional', '2.25', '0.75', 4),
(9, 'sofa', 'vintage', 'Odin Caramel 70\'s sofa', '0.88', '0.15', 7),
(10, 'sofa', 'antique', 'Summerville victorian loveseat', '2.33', '0.80', 8),
(11, 'bed', 'antique', 'Giselle antique dark bronze bedframe', '3.25', '1.00', 9),
(12, 'bed', 'artistic', 'Melina tuffed linen van-gogh', '2.15', '0.75', 10),
(13, 'bookshelf', 'antique', 'Priage victorian bookshelf', '0.75', '0.15', 5),
(14, 'bookshelf', 'modern', 'VECELO modern bookshelf square', '1.33', '0.25', 6),
(15, 'chair', 'rustic', 'PorchRocker country chair', '1.50', '0.25', 11),
(16, 'chair', 'artistic', 'SplatteredPaint kitchen chair', '0.75', '0.15', 12),
(18, 'table', 'traditional', 'Brookside oak dining table large', '1.50', '0.20', 13),
(19, 'table', 'antique', 'Carson antique grain wood dining table', '3.25', '0.95', 14),
(20, 'nightstand', 'rustic', 'CountryLiving ranch style nightstand', '1.10', '0.30', 15);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_item`
--

CREATE TABLE `inventory_item` (
  `inventoryID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_item`
--

INSERT INTO `inventory_item` (`inventoryID`, `quantity`) VALUES
(1, 30),
(2, 15),
(3, 36),
(4, 15),
(5, 23),
(6, 18),
(7, 0),
(8, 5),
(9, 33),
(10, 9),
(11, 1),
(12, 11),
(13, 7),
(14, 5),
(15, 11);

-- --------------------------------------------------------

--
-- Table structure for table `rental_item`
--

CREATE TABLE `rental_item` (
  `rTransID` int(11) NOT NULL,
  `itemID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_item`
--

INSERT INTO `rental_item` (`rTransID`, `itemID`, `quantity`) VALUES
(41, 2, 1),
(42, 1, 1),
(54, 3, 1),
(55, 1, 3),
(55, 3, 5),
(56, 12, 1),
(57, 2, 1),
(57, 11, 1),
(58, 8, 1),
(58, 9, 1),
(58, 11, 1),
(59, 1, 2),
(59, 11, 1),
(59, 15, 1),
(60, 2, 1),
(60, 4, 1),
(61, 2, 2),
(61, 4, 1),
(62, 4, 2),
(62, 8, 1),
(62, 9, 1),
(63, 2, 2),
(63, 5, 4),
(63, 6, 1),
(64, 2, 1),
(64, 4, 1),
(64, 8, 3),
(65, 8, 4),
(65, 9, 3),
(65, 15, 1),
(66, 8, 3),
(66, 9, 3),
(66, 15, 1),
(67, 10, 1),
(67, 11, 1),
(67, 12, 1),
(68, 5, 3),
(68, 11, 2),
(69, 5, 3),
(69, 10, 2),
(69, 11, 2),
(74, 5, 1),
(75, 2, 1),
(75, 8, 1),
(76, 4, 1),
(76, 5, 1),
(77, 11, 1),
(77, 12, 3),
(78, 4, 1),
(78, 6, 2),
(78, 11, 2),
(79, 2, 4),
(79, 10, 1),
(79, 15, 1),
(80, 5, 1),
(80, 11, 2),
(80, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_transaction`
--

CREATE TABLE `rental_transaction` (
  `rentalTransactionID` int(11) NOT NULL,
  `customerID` int(11) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  `rentalDateTime` timestamp NULL DEFAULT NULL,
  `dueDate` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_transaction`
--

INSERT INTO `rental_transaction` (`rentalTransactionID`, `customerID`, `eid`, `rentalDateTime`, `dueDate`) VALUES
(22, 7, 2, '2019-11-18 05:00:00', '2019-12-18 05:00:00'),
(23, 7, 2, '2019-11-18 05:00:00', '2019-12-06 05:00:00'),
(24, 7, 2, '2019-11-18 05:00:00', '2019-11-28 05:00:00'),
(25, 7, 2, '2019-11-18 05:00:00', '2019-11-25 05:00:00'),
(26, 7, 2, '2019-11-19 05:00:00', '2019-11-24 05:00:00'),
(27, 7, 2, '2019-11-19 05:00:00', '2019-11-19 05:00:00'),
(28, 7, 2, '2019-11-19 05:00:00', '2019-11-22 05:00:00'),
(29, 14, 2, '2019-11-20 05:00:00', '2019-11-23 05:00:00'),
(30, 9, 2, '2019-11-20 05:00:00', '2019-12-03 05:00:00'),
(31, 9, 2, '2019-11-20 05:00:00', '2019-12-03 05:00:00'),
(32, 9, 2, '2019-11-20 05:00:00', '2019-11-20 05:00:00'),
(33, 21, 2, '2019-11-20 05:00:00', '2019-11-26 05:00:00'),
(34, 21, 2, '2019-11-20 05:00:00', '2019-11-20 05:00:00'),
(35, 7, 2, '2019-11-20 05:00:00', '2019-11-26 05:00:00'),
(36, 7, 2, '2019-11-20 05:00:00', '2019-11-20 05:00:00'),
(37, 7, 2, '2019-11-20 05:00:00', '2019-11-20 05:00:00'),
(38, 7, 2, '2019-11-20 05:00:00', '2019-11-23 05:00:00'),
(39, 20, 2, '2019-11-20 05:00:00', '2019-11-20 05:00:00'),
(40, 20, 2, '2019-11-20 05:00:00', '2019-11-20 05:00:00'),
(41, 13, 2, '2019-11-20 05:00:00', '2019-11-20 05:00:00'),
(42, 9, 2, '2019-11-21 05:00:00', '2019-11-27 05:00:00'),
(43, 9, 2, '2019-11-21 23:50:55', '2019-11-24 05:00:00'),
(44, 9, 2, '2019-11-21 23:53:31', '2019-11-26 05:00:00'),
(45, 9, 2, '2019-11-22 17:23:26', '2019-11-28 05:00:00'),
(46, 9, 2, '2019-11-22 17:23:26', '2019-11-28 05:00:00'),
(51, 9, 2, '2019-11-22 18:13:15', '2019-11-25 05:00:00'),
(52, 9, 2, '2019-11-22 18:13:15', '2019-11-25 05:00:00'),
(53, 9, 2, '2019-11-22 18:13:15', '2019-11-25 05:00:00'),
(54, 7, 2, '2019-11-22 18:31:35', '2019-11-24 05:00:00'),
(55, 7, 2, '2019-11-22 23:37:17', '2019-11-25 05:00:00'),
(56, 7, 2, '2019-11-25 20:10:25', '2019-11-28 05:00:00'),
(57, 7, 2, '2019-11-25 21:02:25', '2019-12-25 05:00:00'),
(58, 7, 2, '2019-11-26 06:33:07', '2019-11-30 05:00:00'),
(59, 7, 2, '2019-11-26 06:36:53', '2019-12-26 05:00:00'),
(60, 7, 2, '2019-11-27 06:58:15', '2019-12-27 05:00:00'),
(61, 9, 1, '2019-11-27 06:58:15', '2019-12-27 05:00:00'),
(62, 7, 1, '2019-11-27 07:01:07', '2019-12-27 05:00:00'),
(63, 9, 2, '2019-11-27 07:16:55', '2019-12-27 05:00:00'),
(64, 9, 2, '2019-11-27 07:18:10', '2019-12-27 05:00:00'),
(65, 33, 2, '2019-11-27 19:24:39', '2019-12-27 05:00:00'),
(66, 33, 2, '2019-11-27 19:24:39', '2019-12-27 05:00:00'),
(67, 7, 2, '2019-11-27 19:35:15', '2019-12-27 05:00:00'),
(68, 33, 2, '2019-11-27 19:36:23', '2019-12-27 05:00:00'),
(69, 33, 2, '2019-11-27 19:42:10', '2019-12-27 05:00:00'),
(70, 33, 2, '2019-11-27 19:42:10', '2019-12-27 05:00:00'),
(71, 33, 2, '2019-11-27 19:42:10', '2019-12-27 05:00:00'),
(72, 33, 2, '2019-11-27 19:42:10', '2019-12-27 05:00:00'),
(73, 33, 1, '2019-11-27 19:42:10', '2019-12-27 05:00:00'),
(74, 17, 2, '2019-11-27 22:07:25', '2019-11-28 05:00:00'),
(75, 21, 2, '2019-11-30 03:45:33', '2019-12-29 05:00:00'),
(76, 9, 2, '2019-11-30 03:49:40', '2019-12-04 05:00:00'),
(77, 13, 2, '2019-12-01 19:37:52', '2020-03-17 04:00:00'),
(78, 24, 2, '2019-12-01 19:40:52', '2020-02-29 05:00:00'),
(79, 29, 2, '2019-12-01 23:53:32', '2020-02-29 05:00:00'),
(80, 30, 2, '2019-12-01 23:56:25', '2020-05-30 04:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `return_item`
--

CREATE TABLE `return_item` (
  `rentalTransID` int(11) NOT NULL,
  `itemID` int(11) NOT NULL,
  `returnTransID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `return_item`
--

INSERT INTO `return_item` (`rentalTransID`, `itemID`, `returnTransID`, `quantity`) VALUES
(54, 3, 6, 1),
(55, 1, 9, 2),
(55, 1, 12, 1),
(55, 3, 9, 3),
(55, 3, 11, 1),
(55, 3, 13, 1),
(56, 12, 1, 1),
(57, 2, 14, 1),
(57, 11, 14, 1),
(58, 8, 15, 1),
(58, 9, 15, 1),
(59, 1, 17, 1),
(59, 1, 20, 1),
(67, 10, 7, 1),
(67, 10, 8, 1),
(67, 11, 7, 1),
(67, 11, 8, 1),
(67, 12, 7, 1),
(67, 12, 8, 1),
(69, 5, 16, 3),
(69, 10, 16, 2),
(69, 11, 16, 2);

-- --------------------------------------------------------

--
-- Table structure for table `return_transaction`
--

CREATE TABLE `return_transaction` (
  `returnTransactionID` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `returnDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `return_transaction`
--

INSERT INTO `return_transaction` (`returnTransactionID`, `eid`, `returnDateTime`) VALUES
(1, 1, '2019-11-22 12:23:26'),
(2, 2, '2019-11-30 16:08:40'),
(3, 2, '2019-11-30 16:15:36'),
(4, 2, '2019-11-30 16:21:41'),
(5, 2, '2019-11-30 16:59:30'),
(6, 2, '2019-11-30 17:08:36'),
(7, 2, '2019-11-30 17:23:21'),
(8, 2, '2019-11-30 18:40:09'),
(9, 2, '2019-11-30 20:30:02'),
(10, 2, '2019-11-30 20:34:59'),
(11, 2, '2019-11-30 20:40:52'),
(12, 2, '2019-11-30 20:57:46'),
(13, 2, '2019-11-30 21:07:09'),
(14, 2, '2019-11-30 21:09:38'),
(15, 2, '2019-11-30 21:10:45'),
(16, 2, '2019-12-01 11:53:45'),
(17, 2, '2019-12-01 22:24:26'),
(18, 2, '2019-12-01 22:24:36'),
(19, 2, '2019-12-01 22:24:41'),
(20, 2, '2019-12-01 22:31:12'),
(21, 2, '2019-12-01 22:31:21'),
(22, 2, '2019-12-01 22:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `style`
--

CREATE TABLE `style` (
  `name` varchar(20) NOT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `style`
--

INSERT INTO `style` (`name`, `description`) VALUES
('antique', 'Classic style that never go out of fashion'),
('artistic', 'Unique styles for the artistic minded'),
('modern', 'Simple and elegant'),
('rustic', 'Southern country or ranch style'),
('traditional', 'A solid choice'),
('vintage', 'An older style making a comeback');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(16) NOT NULL,
  `password` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`) VALUES
('administrator', 0xc46743beafe65d5f26a29f716a5f4c04),
('fjohnson3', 0xcae3e948337a18b517593a4012e283c0),
('jreed2', 0xd6237847ddbc019ac7ad3799c90a80d6),
('lsanchez1', 0xcfe5b4cc3e0d42ab21088aa8c0691006),
('tbaker1', 0x49c138da14ca2664d0751f59590f3539);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`eID`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `furniture`
--
ALTER TABLE `furniture`
  ADD PRIMARY KEY (`fid`),
  ADD UNIQUE KEY `inventoryID` (`inventoryID`),
  ADD KEY `category` (`category`),
  ADD KEY `style` (`style`);

--
-- Indexes for table `inventory_item`
--
ALTER TABLE `inventory_item`
  ADD PRIMARY KEY (`inventoryID`);

--
-- Indexes for table `rental_item`
--
ALTER TABLE `rental_item`
  ADD PRIMARY KEY (`rTransID`,`itemID`),
  ADD KEY `itemID` (`itemID`);

--
-- Indexes for table `rental_transaction`
--
ALTER TABLE `rental_transaction`
  ADD PRIMARY KEY (`rentalTransactionID`),
  ADD KEY `customerID` (`customerID`),
  ADD KEY `eid` (`eid`);

--
-- Indexes for table `return_item`
--
ALTER TABLE `return_item`
  ADD PRIMARY KEY (`rentalTransID`,`itemID`,`returnTransID`),
  ADD KEY `returnTransID` (`returnTransID`);

--
-- Indexes for table `return_transaction`
--
ALTER TABLE `return_transaction`
  ADD PRIMARY KEY (`returnTransactionID`),
  ADD KEY `eid` (`eid`);

--
-- Indexes for table `style`
--
ALTER TABLE `style`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `cid` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `eID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `furniture`
--
ALTER TABLE `furniture`
  MODIFY `fid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `inventory_item`
--
ALTER TABLE `inventory_item`
  MODIFY `inventoryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `rental_transaction`
--
ALTER TABLE `rental_transaction`
  MODIFY `rentalTransactionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `return_transaction`
--
ALTER TABLE `return_transaction`
  MODIFY `returnTransactionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Constraints for table `furniture`
--
ALTER TABLE `furniture`
  ADD CONSTRAINT `furniture_ibfk_1` FOREIGN KEY (`inventoryID`) REFERENCES `inventory_item` (`inventoryID`),
  ADD CONSTRAINT `furniture_ibfk_2` FOREIGN KEY (`category`) REFERENCES `category` (`name`),
  ADD CONSTRAINT `furniture_ibfk_3` FOREIGN KEY (`style`) REFERENCES `style` (`name`);

--
-- Constraints for table `rental_item`
--
ALTER TABLE `rental_item`
  ADD CONSTRAINT `rental_item_ibfk_1` FOREIGN KEY (`rTransID`) REFERENCES `rental_transaction` (`rentalTransactionID`),
  ADD CONSTRAINT `rental_item_ibfk_2` FOREIGN KEY (`itemID`) REFERENCES `inventory_item` (`inventoryID`);

--
-- Constraints for table `rental_transaction`
--
ALTER TABLE `rental_transaction`
  ADD CONSTRAINT `rental_transaction_ibfk_1` FOREIGN KEY (`customerID`) REFERENCES `customer` (`cid`),
  ADD CONSTRAINT `rental_transaction_ibfk_2` FOREIGN KEY (`eid`) REFERENCES `employee` (`eID`);

--
-- Constraints for table `return_item`
--
ALTER TABLE `return_item`
  ADD CONSTRAINT `return_item_ibfk_1` FOREIGN KEY (`rentalTransID`,`itemID`) REFERENCES `rental_item` (`rTransID`, `itemID`),
  ADD CONSTRAINT `return_item_ibfk_2` FOREIGN KEY (`returnTransID`) REFERENCES `return_transaction` (`returnTransactionID`);

--
-- Constraints for table `return_transaction`
--
ALTER TABLE `return_transaction`
  ADD CONSTRAINT `return_transaction_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `employee` (`eID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
